import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { TagInputModule } from 'ngx-chips';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; 
import { WebcamModule } from 'ngx-webcam';
// import * as HighCharts from 'highcharts';
import { ChartModule } from 'angular-highcharts';
import { MyApp } from './app.component';

// native providers
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { NativePageTransitions } from '@ionic-native/native-page-transitions';
import { Facebook } from '@ionic-native/facebook';
import { TwitterConnect } from '@ionic-native/twitter-connect';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { Camera } from '@ionic-native/camera';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Clipboard } from '@ionic-native/clipboard';
import { FileTransfer } from '@ionic-native/file-transfer';
import { FilePath } from '@ionic-native/file-path';
import { File } from '@ionic-native/file';

// custom providers
import { ApiService, ConfigService, CommonService, AuthService, OauthService, UserService, DashboardService, StoryService, TeamService, TimelineService, CameraService, SharedService, StorageService, LoaderService, ToastService } from './../providers';

// import { ImageByNameDirective } from './../directives/image-by-name/image-by-name';

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpModule,
    TagInputModule,
    BrowserAnimationsModule,
    WebcamModule,
    // HighCharts,
    ChartModule,
    IonicModule.forRoot(MyApp, {
      statusbarPadding: false,
      tabsHideOnSubPages: true,
      backButtonText: '',
      backButtonIcon: 'arrow-round-back',
      iconMode: 'md',
      pageTransition: 'ios'
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    StatusBar,
    SplashScreen,
    NativePageTransitions,
    Facebook,
    TwitterConnect,
    InAppBrowser,
    Camera,
    SocialSharing,
    Clipboard,
    FileTransfer,
    FilePath,
    File,
    ApiService, 
    ConfigService, 
    CommonService,
    AuthService,
    OauthService,
    UserService, 
    DashboardService,
    StoryService, 
    TeamService, 
    TimelineService,
    CameraService, 
    SharedService, 
    StorageService, 
    LoaderService, 
    ToastService,
    { provide: ErrorHandler, useClass: IonicErrorHandler }
  ]
})
export class AppModule {}
