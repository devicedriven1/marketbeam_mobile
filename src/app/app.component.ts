import { Component, ViewChild } from '@angular/core';
import { Nav, App, Platform, Events, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AuthService, StorageService, LoaderService, ToastService } from './../providers';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  @ViewChild('rootNav') nav: Nav;
  rootPage: any = 'TabsPage';
  counter: number = 0;

  /**
   * constructor
   *
   * @param {type}  public app: App      
   * @param {type}  public platform: Platform  
   * @param {type}  public events: Events
   * @param {type}  public alertCtrl: AlertController           
   * @param {type}  public statusBar: StatusBar 
   * @param {type}  public splashScreen: SplashScreen  
   * @param {type}  private storage: StorageService   
   * @param {type}  private toast: ToastService  
   */
  constructor(public app: App, public platform: Platform, public events: Events, public alertCtrl: AlertController, public statusBar: StatusBar, public splashScreen: SplashScreen, private auth: AuthService, private storage: StorageService, private loader: LoaderService, private toast: ToastService) {
    this.initializeApp();
    this.checkUserloggedIn();
    events.subscribe('user:logout', () => {
      this.doLogout();
    });
  }

  /**
   * initializeApp
   *
   * set statusbar style
   * hide the splashscreen
   * 
   * Handle the register back button actions
   * if previous page exist navigate to previous screen 
   * otherwise navigate to dashboard screen
   * also if user double tap on dashboard screen 
   * show confirmation to exit from app
   */
  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      // this.statusBar.overlaysWebView(true);
      // statusBar.styleDefault();
      this.statusBar.styleLightContent();
      this.statusBar.backgroundColorByHexString('#033772');
      this.splashScreen.hide();
    });

    // register back button actions
    this.platform.registerBackButtonAction(() => {
     
      // Catches the active view
      let nav = this.app.getActiveNavs()[0];
      let activeView = nav.getActive();  

      // Checks if can go back before show up the alert
      if(activeView.name === 'TabsPage') {
        if(this.counter == 0) {

          // show toast while user click on back button from dashboard screen
          this.counter++;
          this.toast.custom('Press again to exit');
          setTimeout(() => { 
            this.counter = 0 
          }, 3000);

        } else {

          // confirmation to exit from app
          const alert = this.alertCtrl.create({
            title: 'Market Beam',
            message: 'Are you sure you want to exit the app?',
            buttons: [{
              text: 'Yes',
              handler: () => {
                this.platform.exitApp();
              }
            }, {
              text: 'Cancel',
              role: 'cancel',
              handler: () => {
                this.rootPage = 'TabsPage';
              }
            }]
          });
          alert.present();
        }
      } else {
        if (nav.canGoBack()){
          nav.pop();
        }
      }
    });
  }

  /**
   * check user is logged in
   *
   * if user is logged in set root as dashboard screen
   * if not set root as login screen
   */
  checkUserloggedIn() {
    if(this.storage.get('isLoggedIn') == '' ||
      this.storage.get('isLoggedIn') == null ||
      this.storage.get('isLoggedIn') == undefined) {
      this.rootPage = 'LoginPage';
    } else {
      // let user = JSON.parse(this.storage.get("userData"));
      this.rootPage = 'TabsPage';
    }
  }

  /**
   * logout from application
   *
   * while user logout please clear | remove all localStorage data
   * after clear please navigate to login screen
   */
  doLogout() {
    this.storage.remove('isLoggedIn');
    this.storage.remove('authToken');
    this.storage.remove('userData');
    this.rootPage = 'LoginPage';
  }

}
