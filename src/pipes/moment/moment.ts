import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
	name: 'moment'
})
export class MomentPipe implements PipeTransform {
  
	/**
	 * Transform date format
	 *
	 * @param  {type}  date
	 * @param  {type}  format
	 * @return {type}  formatted date
	 */
	transform(date, format) {
		return moment(date).format(format);
	}

}
