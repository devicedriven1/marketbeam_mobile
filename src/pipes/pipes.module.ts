import { NgModule } from '@angular/core';
import { MomentPipe } from './moment/moment';
import { ToFixedPipe } from './to-fixed/to-fixed';

@NgModule({
	declarations: [
		MomentPipe,
	    ToFixedPipe
    ],
	imports: [],
	exports: [
		MomentPipe,
	    ToFixedPipe
    ]
})
export class PipesModule {}
