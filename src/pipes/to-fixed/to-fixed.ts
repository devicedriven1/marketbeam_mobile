import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ 
	name: 'toFixed'
})
export class ToFixedPipe implements PipeTransform {
    
    /**
	 * Transform date format
	 *
	 * @param  {type}  value
	 * @return {type}  formatted number
	 */
	transform(value: any, ...args) {
		return Number(value).toFixed(2); 
	}

}
