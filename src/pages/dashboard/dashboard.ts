import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { CommonService, DashboardService, StorageService, LoaderService } from './../../providers';
import moment from 'moment';
import * as HighCharts from 'highcharts';

@IonicPage()
@Component({
	selector: 'page-dashboard',
	templateUrl: 'dashboard.html'
})
export class DashboardPage {
    
    public activeTab: any;
    private teamId: any;
    public topEmployeeOf: any;
    public trends: any[];
    public bestChannel = {
        name: "",
        hits: 0,
        shares: 0
    };
    public bestChannels: any[];
    public leaderboard: any[];
    public topPosts: any[];
    public clickThroughs: any[];
    public dashboardStats = {
        nvalue_earned: 0,
        nshares: 0,
        nviews: 0,
        nmembers: 0,
        npoints: 0,
        nposts: 0
    };
    public topEmployee = {
        id: null, 
        email: '',
        firstname: '',
        lastname: '',
        profileImage: '',
        profileWord: '', 
        nclicks: 0, 
        npoints: 0, 
        nshares: 0 
    };
    public avgPost = {
        npostlastweek: 0
    };
    public lastPost = {
        days: 0,
        hours: 0
    };
    public companyChannels = {
        facebookpage: "no",
        linkedinpage: "no"
    };

    /**
     * constructor
     *
     * @param {type}  public navCtrl: NavController  
     * @param {type}  public commonService: CommonService
     * @param {type}  private dashboardService: DashboardService
     * @param {type}  private storage: StorageService
     * @param {type}  private loader: LoaderService
     */
	constructor(public navCtrl: NavController, public commonService: CommonService, private dashboardService: DashboardService, private storage: StorageService, private loader: LoaderService) {
    }
    
    /**
     * ionViewDidLoad
     *
     * initialize dashboard screen
     * set teamId
     *
     */
	ionViewDidLoad() {
        let user = JSON.parse(this.storage.get('userData'));
        this.teamId = user.teamId;

        this.activeTab = "Summary";

        this.getSummary();
        this.getTrends();
        this.getBestChannel();
        this.getLeaderboard();
        this.getEmployeeOfMonth();
        this.getTopPosts();
        this.getClickThroughsByCountry();
        this.getAvgPost();
        this.getTimeSinceLastPost();
        this.getCompanyChannels();
	}

    /**
     * onSwipe
     *
     * On tab swipe redirect to pages
     *
     * @param  {type}  event
     */
    onSwipe(event) {
        if(event.direction === 2) {
            this.navCtrl.parent.select(1);
        } 
    }
    
    /**
     * getTab
     *
     * @param  {type}  tab
     */
    getTab(tab: any) {
        this.activeTab = tab;
        if(this.activeTab == 'Engagement') {
            let channels = [];
            let clickThroughs = [];
            for(let i = 0; i < this.bestChannels.length; i++) {
                if(channels.length == 0) {
                    channels.push({
                        name: this.bestChannels[i].name,
                        y: this.bestChannels[i].shares,
                        sliced: true,
                        selected: true
                    }) 
                } else {
                    channels.push({
                        name: this.bestChannels[i].name,
                        y: this.bestChannels[i].shares
                    })
                }

                if(clickThroughs.length == 0) {
                    clickThroughs.push({
                        name: this.bestChannels[i].name,
                        y: this.bestChannels[i].hits,
                        sliced: true,
                        selected: true
                    }) 
                } else {
                    clickThroughs.push({
                        name: this.bestChannels[i].name,
                        y: this.bestChannels[i].hits
                    })
                }
            }

            setTimeout( () => {
                let chartOptions = {
                    type: 'pie'
                };

                let plotOptions = {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                            style: {
                                color: 'black'
                            }
                        }
                    }
                };
                
                HighCharts.chart('best-channel-shares', {
                    chart: chartOptions,
                    title: { text: 'Shares' },
                    tooltip: { pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>' },
                    plotOptions: plotOptions,
                    credits: { enabled: false },
                    series: [{
                        type: 'pie',
                        name: 'Shares',
                        colorByPoint: true,
                        data: channels
                    }]
                });

                HighCharts.chart('clickthroughs-country', {
                    chart: chartOptions,
                    title: { text: 'Clickthroughs' },
                    tooltip: { pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>' },
                    plotOptions: plotOptions,
                    credits: { enabled: false },
                    series: [{
                        type: 'pie',
                        name: 'Clickthroughs',
                        colorByPoint: true,
                        data: clickThroughs
                    }]
                });
            }, 1500);
        }
    }

    /**
     * getSummary
     *
     * get summary cards
     */
    getSummary() {
        this.loader.show();
        this.dashboardService.getSummary(this.teamId).subscribe((response) => {
            this.loader.hide();
            if(response) {
                this.dashboardStats = response.objects;
            }
        }, (error) => {
            this.loader.hide();
        });
    }

    /**
     * getTrends
     *
     * 30 days Trend: Shares Vs Cumulative Clicks
     */
    getTrends() {
        let days = 30
        this.loader.show();
        this.dashboardService.getTrends(this.teamId, days).subscribe((response) => {
            this.loader.hide();
            if(response) {
                this.trends = response.objects;
            }
        }, (error) => {
            this.loader.hide();
        });
    }

    /**
     * getBestChannel
     *
     * get best engaging channel
     */
    getBestChannel() {
        this.loader.show();
        this.dashboardService.getBestChannel(this.teamId).subscribe((response) => {
            this.loader.hide();
            if(response) {
                this.bestChannels = response.objects;
                this.bestChannel  = response.objects[0];
            }
        }, (error) => {
            this.loader.hide();
        });
    }

    /**
     * getLeaderboard
     *
     * get leaderboard
     */
    getLeaderboard() {
        this.loader.show();
        this.dashboardService.getLeaderboard(this.teamId).subscribe((response) => {
            this.loader.hide();
            if(response) {
                this.leaderboard = response.objects;
            }
        }, (error) => {
            this.loader.hide();
        });
    }

    /**
     * getEmployeeOfMonth
     *
     * get social employee of the month
     */
    getEmployeeOfMonth() {
        let today = moment(new Date(), 'YYYY/MM/DD');
        let month = today.format('M');
        let year  = today.format('YYYY');
        this.topEmployeeOf = today.format('MMMM') + ' ' + year;
        this.loader.show();
        this.dashboardService.getEmployeeOfMonth(this.teamId, month, year).subscribe((response) => {
            this.loader.hide();
            if(response) {
                this.topEmployee = response.objects[0];
            }
        }, (error) => {
            this.loader.hide();
        });
    }

    /**
     * getTopPosts
     *
     * get top post
     */
    getTopPosts() {
        this.loader.show();
        this.dashboardService.getTopPosts(this.teamId).subscribe((response) => {
            this.loader.hide();
            if(response) {
                this.topPosts = response.objects;
            }
        }, (error) => {
            this.loader.hide();
        });
    }

    /**
     * getClickThroughsByCountry
     *
     * get click throughs by country
     */
    getClickThroughsByCountry() {
        this.loader.show();
        this.dashboardService.getClickThroughsByCountry(this.teamId).subscribe((response) => {
            this.loader.hide();
            if(response) {
                this.clickThroughs = response.result;
            }
        }, (error) => {
            this.loader.hide();
        });
    }

    /**
     * getAvgPost
     *
     * get average post
     */
    getAvgPost() {
        this.loader.show();
        this.dashboardService.getAvgPost(this.teamId).subscribe((response) => {
            this.loader.hide();
            if(response) {
                this.avgPost = response.objects;
            }
        }, (error) => {
            this.loader.hide();
        });
    }

    /**
     * getTimeSinceLastPost
     *
     * get time since last post
     */
    getTimeSinceLastPost() {
        this.loader.show();
        this.dashboardService.getTimeSinceLastPost(this.teamId).subscribe((response) => {
            this.loader.hide();
            if(response) {
                this.lastPost = response.objects;
            }
        }, (error) => {
            this.loader.hide();
        });
    }

    /**
     * getCompanyChannels
     *
     * get company channels
     */
    getCompanyChannels() {
        this.loader.show();
        this.dashboardService.getCompanyChannels(this.teamId).subscribe((response) => {
            this.loader.hide();
            if(response) {
                this.companyChannels = response.objects;
            }
        }, (error) => {
            this.loader.hide();
        });
    }

    /**
     * getStory
     *
     * get story in details
     * navigate to story screen
     * set navaparams as storyid
     *
     * @param  {type}  item
     */
    getStory(item: any) {
        this.navCtrl.push('StoryPage', {
            id: item.id
        })
    }

    /**
     * getTeam
     *
     * navigate to team page
     */
    getTeam() {
        this.navCtrl.push('TeamPage')
    }

    /**
     * connectChannel
     *
     * navigate to profile page
     */
    connectChannel() {
        this.navCtrl.push('ProfilePage')
    }

}
