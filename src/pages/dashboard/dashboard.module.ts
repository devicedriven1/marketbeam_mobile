import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DashboardPage } from './dashboard';
import { ChartModule } from 'angular-highcharts';

@NgModule({
	declarations: [
		DashboardPage
	],
	imports: [
	ChartModule,
		IonicPageModule.forChild(DashboardPage)
	]
})
export class DashboardPageModule {}
