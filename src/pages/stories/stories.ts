import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, ModalController, Content } from 'ionic-angular';
import { CommonService, StoryService, StorageService, LoaderService } from './../../providers';

@IonicPage()
@Component({
	selector: 'page-stories',
	templateUrl: 'stories.html'
})
export class StoriesPage {

	@ViewChild('pageTop') pageTop: Content;
	public showEmptyPage: boolean = false;
	private scrollToTopEnabled: boolean = true;
	public isInfiniteEnable: boolean = true;
	public activeTab: any;
	private offset: number = 0;
	private teamId: any;
	public stories: any[];

	/**
     * constructor
     *
     * @param {type}  public navCtrl: NavController  
     * @param {type}  public modalCtrl: ModalController
     * @param {type}  public commonService: CommonService
     * @param {type}  private storyService: StoryService
     * @param {type}  private storage: StorageService
     * @param {type}  private loader: LoaderService
     */
	constructor(public navCtrl: NavController, public modalCtrl: ModalController, public commonService: CommonService, private storyService: StoryService, private storage: StorageService, private loader: LoaderService) {
	}
    
    /**
     * ionViewDidLoad
     */
	ionViewDidLoad() {
		let user    = JSON.parse(this.storage.get('userData'));
		this.teamId = user.teamId;
		this.getStories('published');
	}

	/**
	 * onSwipe
	 *
     * On tab swipe redirect to pages
     *
     * @param  {type}  event
     */
	onSwipe(event) {
		if(event.direction === 2 && this.activeTab == 'published') {
			this.getStories('scheduled');
		} else if(event.direction === 4 && this.activeTab == 'published') {
			this.navCtrl.parent.select(0);
		} else if(event.direction === 2 && this.activeTab == 'scheduled') {
			this.getStories('pending');
		} else if(event.direction === 4 && this.activeTab == 'scheduled') {
			this.getStories('published');
		} else if(event.direction === 2 && this.activeTab == 'pending') {
			this.navCtrl.parent.select(2);
		} else if(event.direction === 4 && this.activeTab == 'pending') {
			this.getStories('scheduled');
		}
    }
    
    /**
     * Get stories
     *
     * @param  {type}  storyType
     * @return {type}  stories
     */
	getStories(storyType: any) {
		this.offset = 0;
		this.showEmptyPage = false;
		this.isInfiniteEnable = true;
		this.activeTab = storyType;
        this.stories = [];

        let param = {
		    team_id: this.teamId,
		    state: this.activeTab,
		    offset: this.offset
		};
        this.loader.show();
        this.storyService.getAll(param).subscribe((response) => {
            this.loader.hide();
            this.showEmptyPage = true;
            if(response.status) {
            	this.stories = response.data;
            	if (response.data.length < 10) {
			        this.isInfiniteEnable = false;
		        } else {
			        this.offset = this.offset + 10;
		        }
            }
        }, (error) => {
            this.loader.hide();
            this.showEmptyPage = true;
        });
		
		if(this.scrollToTopEnabled) {
			this.scrollToTopEnabled = false;
			this.pageTop.scrollToTop(500);
			setTimeout(() => {
				this.scrollToTopEnabled = true;
			}, 500)
		}
	}
    
    /**
	 * doInfinite
	 *
     * Get list on page scroll
     *
     * @param  {type}  infiniteScroll
     */
	doInfinite(infiniteScroll): Promise<any> {
        return new Promise((resolve) => {
		    setTimeout(() => {
				let param = {
				    team_id: this.teamId,
				    state: this.activeTab,
				    offset: this.offset
				};
		        this.loader.show();
		        this.storyService.getAll(param).subscribe((response) => {
		            this.loader.hide();
		            if(response.status == true) {
						if (response.data.length < 10) {
							this.isInfiniteEnable = false;
						} else {
							this.offset = this.offset + 10;
						}
						this.stories = this.stories.concat(response.data);
					}
		        }, (error) => {
		            this.loader.hide();
		        });
				infiniteScroll.complete();
		    }, 500);
		})
	}
    
    /**
	 * doRefresh
	 *
     * List refresh on each tab
     *
     * @param  {type}  refresher
     */
	doRefresh(refresher) {
		setTimeout(() => {
			this.offset = 0;
			this.getStories(this.activeTab);
			refresher.complete();
		}, 2000);
	}
    
    /**
	 * openStoryModal
	 *
     * Open story modal to create new | post story
     */
	openStoryModal() {
		const modal = this.modalCtrl.create('StoryModalPage', {
			title: 'Create story' 
		});
        modal.present();
        modal.onDidDismiss(data => {
        	if(data == 'created') {
			    this.offset = 0;
			    this.activeTab = 'published';
			    this.getStories(this.activeTab);
			}
	    });
	}
    
    /**
	 * getStory
	 *
     * Get story in details
     * navigate to story screen
     * set navaparams as storyid
     *
     * @param  {type}  item
     */
	getStory(item: any) {
        this.navCtrl.push('StoryPage', {
        	id: item.post_id
        })
	}
    
    /**
	 * doCopy
	 *
     * Duplicate story
     *
     * @param  {type}  item
     */
	doCopy(item: any) {
		const modal = this.modalCtrl.create('StoryModalPage', { 
			id: item.post_id,
			title: 'Duplicate story' 
		});
        modal.present();
	}
    
    /**
	 * doShare
	 *
     * Share story
     *
     * @param  {type}  item
     */
	doShare(item: any) {
		const modal = this.modalCtrl.create('ShareStoryModalPage', { 
			id: item.post_id 
		});
		modal.present();
	}

}
