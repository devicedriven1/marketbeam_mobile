import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';

@IonicPage()
@Component({
	selector: 'page-tabs',
	templateUrl: 'tabs.html'
})
export class TabsPage {

	loaded: boolean = false;
    tabIndex: number = 0;

	public tab1Root = 'DashboardPage';
	public tab2Root = 'StoriesPage';
	public tab3Root = 'TimelinesPage';
	public tab4Root = 'ProfilePage';
	public tab5Root = 'TeamPage';
	public tab6Root = 'SettingsPage';

	constructor(private nativePageTransitions: NativePageTransitions) {
	}

	private getAnimationDirection(index):string {
		var currentIndex = this.tabIndex;

		this.tabIndex = index;

		switch (true){
		    case (currentIndex < index):
			    return('left');
		    case (currentIndex > index):
			    return ('right');
		}
	}

	public transition(e):void {
		let options: NativeTransitionOptions = {
			direction: this.getAnimationDirection(e.index),
			duration: 250,
			slowdownfactor: -1,
			slidePixels: 0,
			iosdelay: 20,
			androiddelay: 0,
			fixedPixelsTop: 0,
			fixedPixelsBottom: 48
		};

		if (!this.loaded) {
			this.loaded = true;
			return;
		}

		this.nativePageTransitions.slide(options);
	}
  
}
