import { Component } from '@angular/core';
import { IonicPage, ViewController } from 'ionic-angular';

@IonicPage()
@Component({
	selector: 'page-user-filter-popover',
	templateUrl: 'user-filter-popover.html'
})
export class UserFilterPopoverPage {

    public filters: any[];

	/**
     * constructor
     *
     * @param {type}  public viewCtrl: ViewController
     */
	constructor(public viewCtrl: ViewController) {
	}
    
    /**
     * ionViewDidLoad
     */
	ionViewDidLoad() {
        this.filters = [
            { title: 'All Users', value: 'all' },
            { title: 'Users waiting for Approval', value: 'toApprove' },
            { title: 'Administrators', value: 'admins' },
            { title: 'Normal Users', value: 'normal' },
            { title: 'Users with no Activity', value: 'noActivity' },
            { title: 'Users not joined yet', value: 'notJoined' },
            { title: 'Users with no active accounts', value: 'noAccounts' }
        ];
	}
    
    /**
     * Close popover
     *
     * @param {type}  filter
     */
	close(filter: any) {
		this.viewCtrl.dismiss(filter);
	}

}
