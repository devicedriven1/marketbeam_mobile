import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserFilterPopoverPage } from './user-filter-popover';

@NgModule({
	declarations: [
		UserFilterPopoverPage
	],
	imports: [
		IonicPageModule.forChild(UserFilterPopoverPage)
	]
})
export class UserFilterPopoverPageModule {}
