import { Component } from '@angular/core';
import { IonicPage, ViewController } from 'ionic-angular';

@IonicPage()
@Component({
	selector: 'page-timeline-filter-popover',
	templateUrl: 'timeline-filter-popover.html'
})
export class TimelineFilterPopoverPage {

    public filters: any[];

	/**
     * constructor
     *
     * @param {type}  public viewCtrl: ViewController
     */
	constructor(public viewCtrl: ViewController) {
	}
    
    /**
     * ionViewDidLoad
     */
	ionViewDidLoad() {
        this.filters = [
            { title: 'Clicked & Shared', value: 'hit,share' },
            { title: 'Clicked', value: 'hit' },
            { title: 'Shared', value: 'share' },
            { title: 'Posted', value: 'post' },
            { title: 'All', value: 'hit,post,share' }
        ];
	}
    
    /**
     * Close popover
     *
     * @param {type}  filter
     */
	close(filter: any) {
		this.viewCtrl.dismiss(filter);
	}

}
