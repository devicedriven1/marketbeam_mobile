import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TimelineFilterPopoverPage } from './timeline-filter-popover';

@NgModule({
	declarations: [
		TimelineFilterPopoverPage
	],
	imports: [
		IonicPageModule.forChild(TimelineFilterPopoverPage)
	]
})
export class TimelineFilterPopoverPageModule {}
