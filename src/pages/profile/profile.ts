import { Component } from '@angular/core';
import { IonicPage, NavController, ModalController } from 'ionic-angular';
import { UserService, StorageService, LoaderService, ToastService } from './../../providers';

@IonicPage()
@Component({
	selector: 'page-profile',
	templateUrl: 'profile.html'
})
export class ProfilePage {
    
    public showPage: boolean = false;
    private teamId: any;
    public channels: any[];
    public user = {
    	image: '',
    	firstname: '',
		lastname: '',
		email: '',
		autoshare: false,
		enable_fb_share: false,
		complete: false
    };

	/**
     * constructor
     *
     * @param {type}  public navCtrl: NavController  
     * @param {type}  public modalCtrl: ModalController
     * @param {type}  private userService: UserService
     * @param {type}  private storage: StorageService
     * @param {type}  private loader: LoaderService
     */
	constructor(public navCtrl: NavController, public modalCtrl: ModalController, private userService: UserService, private storage: StorageService, private loader: LoaderService, private toast: ToastService) {
	}
    
    /**
     * ionViewDidLoad
     */
	ionViewDidLoad() {
        let user = JSON.parse(this.storage.get('userData'));
        this.teamId = user.teamId;

		this.getProfile();
		this.getChannels();
	}

	/**
	 * onSwipe
	 *
     * On tab swipe redirect to pages
     *
     * @param  {type}  event
     */
	onSwipe(event) {
		if(event.direction === 2) {
			this.navCtrl.parent.select(4);
		} 
		if(event.direction === 4) {
			this.navCtrl.parent.select(2);
		}
    }
    
    /**
	 * get user profile
     */
    getProfile() {
        this.showPage = false;
        this.loader.show();
        this.userService.get().subscribe((response) => {
            this.loader.hide();
            this.showPage = true;
            if(response) {
            	this.user = {
		    		image: '',
		    		firstname: response.firstname,
					lastname: response.lastname,
					email: response.email,
		    		autoshare: response.autoshare,
					enable_fb_share: response.enable_fb_share,
					complete: response.complete
		    	};
            }
        }, (error) => {
            this.loader.hide();
            this.showPage = true;
        });
    }
    
    /**
	 * get channels
     */
    getChannels() {
    	this.loader.show();
        this.userService.getAllChannel().subscribe((response) => {
            this.loader.hide();
            if(response.objects) {
            	this.channels = response.objects;
                if(response.objects.length > 0) {
                    this.user.image = response.objects[0].image_url;
                }

                this.userService.getAllSubChannel().subscribe((response) => {
                    if(response.objects) {
                        let subChannelResponse = response.objects
                        for(let i = 0; i < subChannelResponse.length; i++) {
                            this.channels.push({
                                connect_url: subChannelResponse[i].disconnect_url,
                                connected: true,
                                disconnect_url: subChannelResponse[i].disconnect_url,
                                image_url: "",
                                name: subChannelResponse[i].name,
                                profile_url: "",
                                provider_user_id: subChannelResponse[i].provider_user_id,
                                user_name: subChannelResponse[i].subchannel_name,
                                subchannel_id: subChannelResponse[i].subchannel_id,
                                type: 'subchannel'
                            })
                        }
                    }
                }, (error) => {
                });

                this.userService.getAllSubChannelMeta().subscribe((response) => {
                    if(response.objects) {
                        let subChannelMetaReponse = response.objects;
                        for(let i = 0; i < subChannelMetaReponse.length; i++) {
                            this.channels.push({
                                connect_url: subChannelMetaReponse[i].connect_url,
                                connected: subChannelMetaReponse[i].connected,
                                disconnect_url: subChannelMetaReponse[i].disconnect_url,
                                image_url: "",
                                name: subChannelMetaReponse[i].name,
                                profile_url: "",
                                provider_user_id: subChannelMetaReponse[i].provider_user_id,
                                user_name: subChannelMetaReponse[i].user_name,
                                type: 'subchannelmeta'
                            })
                        }
                    }
                }, (error) => {
                });

            }
        }, (error) => {
            this.loader.hide();
        });
    }

    /**
     * getSubChannels
     */
    getSubChannels() {
        this.loader.show();
        this.userService.getAllSubChannel().subscribe((response) => {
            this.loader.hide();
            if(response.objects) {
                for(let i = 0; i < response.length; i++) {
                    this.channels.push({
                        connect_url: response[i].disconnect_url,
                        connected: true,
                        disconnect_url: response[i].disconnect_url,
                        image_url: "",
                        name: response[i].name,
                        profile_url: "",
                        provider_user_id: response[i].provider_user_id,
                        user_name: response[i].subchannel_name,
                        subchannel_id: response[i].subchannel_id
                    })
                }
            }
        }, (error) => {
            this.loader.hide();
        });
    }

    /**
     * getSubChannelMeta
     */
    getSubChannelMeta() {
        this.loader.show();
        this.userService.getAllSubChannelMeta().subscribe((response) => {
            this.loader.hide();
            if(response.objects) {
                for(let i = 0; i < response.length; i++) {
                    this.channels.push({
                        connect_url: response[i].connect_url,
                        connected: response[i].connected,
                        disconnect_url: response[i].disconnect_url,
                        image_url: "",
                        name: response[i].name,
                        profile_url: "",
                        provider_user_id: response[i].provider_user_id,
                        user_name: response[i].user_name
                    })
                }
            }
        }, (error) => {
            this.loader.hide();
        });
    }

    /**
     * openModal
     *
     * @param  {type}  page
     * @param  {type}  fab
     */
    openModal(page, fab) {
        fab.close();
        const modal = this.modalCtrl.create(page);
        modal.present();
        modal.onDidDismiss((data) => {
            if(data == 'success') {
                this.getProfile();
                this.getChannels();
            }
        });
    }
    
    /**
	 * disconnection
	 *
	 * disconnect social media connections
	 * logout from social media
	 * remove | clear the user profile details saved in localStorage
     *
     * @param  {type}  item
     */
    disconnect(item){
    	this.loader.show();
        this.userService.deleteConnection(item.name, item.provider_user_id).subscribe((response) => {
            this.loader.hide();
            if(response.status == true) {
                this.toast.custom(response.message);
                this.getChannels();
            }
        }, (error) => {
            this.loader.hide();
        });
	}

}
