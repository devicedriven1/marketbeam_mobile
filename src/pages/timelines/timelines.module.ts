import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TimeAgoPipe } from 'time-ago-pipe';
import { TimelinesPage } from './timelines';
// import { ImageByNameDirective } from './../../directives/image-by-name/image-by-name';

@NgModule({
  declarations: [
		TimeAgoPipe,
		TimelinesPage,
		// ImageByNameDirective
	],
	imports: [
		IonicPageModule.forChild(TimelinesPage)
	]
})
export class TimelinesPageModule {}
