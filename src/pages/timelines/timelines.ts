import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, PopoverController, Content } from 'ionic-angular';
import { CommonService, TimelineService, StorageService, LoaderService } from './../../providers';

@IonicPage()
@Component({
	selector: 'page-timelines',
	templateUrl: 'timelines.html'
})
export class TimelinesPage {

	@ViewChild('pageTop') pageTop: Content;
	public showEmptyPage: boolean = false;
	private scrollToTopEnabled: boolean = true;
	public isInfiniteEnable: boolean = true;
	private offset: number = 0;
	private teamId: any;
	private filterType: any;
	public items: any[];
	public epoch: any;

	/**
     * constructor
     *
     * @param {type}  public navCtrl: NavController 
     * @param {type}  public popoverCtrl: PopoverController
     * @param {type}  public commonService: CommonService
     * @param {type}  private timelineService: TimelineService
     * @param {type}  private storage: StorageService
     * @param {type}  private loader: LoaderService
     */
	constructor(public navCtrl: NavController, public popoverCtrl: PopoverController, public commonService: CommonService, private timelineService: TimelineService, private storage: StorageService, private loader: LoaderService) {
	}
    
    /**
     * ionViewDidLoad
     */
	ionViewDidLoad() {
		let user    = JSON.parse(this.storage.get('userData'));
        this.teamId = user.teamId;
        this.filterType = 'hit,post,share';
		this.getTimelines();
	}

	/**
	 * onSwipe
	 *
     * On tab swipe redirect to pages
     *
     * @param  {type}  event
     */
	onSwipe(event) {
		if(event.direction === 2) {
			this.navCtrl.parent.select(3);
		} 
		if(event.direction === 4) {
			this.navCtrl.parent.select(1);
		}
    }

    /**
	 * openFilters
	 *
	 * @return {type}  event
	 */
    openFilters(event) {
		let popover = this.popoverCtrl.create('TimelineFilterPopoverPage');
		popover.present({
			ev: event
		});
		popover.onDidDismiss((data) => {
			if(data) {
				this.filterType = data.value;
				this.getTimelines();
			} 
		});
	}

    /**
     * Get stories
     *
     * @return {type}  timelines
     */
	getTimelines() {
		this.showEmptyPage = false;
		this.isInfiniteEnable = true;
		this.offset = 0;
        this.items = [];
        let param = {
		    teamId: this.teamId,
		    types: this.filterType,
		    offset: this.offset
		};
        this.loader.show();
        this.timelineService.getAll(param).subscribe((response) => {
            this.loader.hide();
            this.showEmptyPage = true;
            if(response) {
            	this.items = response.objects;
            	this.epoch = response.objects[response.objects.length-1].epoch;
            	if (response.objects.length < 10) {
			        this.isInfiniteEnable = false;
		        } else {
			        this.offset = this.offset + 10;
		        }
            }
        }, (error) => {
            this.loader.hide();
            this.showEmptyPage = true;
        });
		
		if(this.scrollToTopEnabled) {
			this.scrollToTopEnabled = false;
			this.pageTop.scrollToTop(500);
			setTimeout(() => {
				this.scrollToTopEnabled = true;
			}, 500)
		}
	}
    
    /**
	 * doInfinite
	 *
     * Get list on page scroll
     *
     * @param  {type}  infiniteScroll
     */
	doInfinite(infiniteScroll): Promise<any> {
        return new Promise((resolve) => {
		    setTimeout(() => {
				let param = {
				    teamId: this.teamId,
				    types: "post,share,hit",
				    offset: this.offset,
				    epoch: this.epoch
				};
		        this.loader.show();
		        this.timelineService.getAll(param).subscribe((response) => {
		            this.loader.hide();
		            if(response) {
						if (response.objects.length < 10) {
							this.isInfiniteEnable = false;
						} else {
							this.offset = this.offset + 10;
						}
						this.epoch = "";
						this.items = this.items.concat(response.objects);
						this.epoch = this.items[this.items.length-1].epoch;
					}
		        }, (error) => {
		            this.loader.hide();
		        });
				infiniteScroll.complete();
		    }, 500);
		})
	}
    
    /**
	 * doRefresh
	 *
     * List refresh on each tab
     *
     * @param  {type}  refresher
     */
	doRefresh(refresher) {
		setTimeout(() => {
			this.offset = 0;
			this.getTimelines();
			refresher.complete();
		}, 2000);
	}

}
