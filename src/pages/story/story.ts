import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { StoryService, LoaderService } from './../../providers';
import { Story } from './../../models/story';

@IonicPage()
@Component({
	selector: 'page-story',
	templateUrl: 'story.html'
})
export class StoryPage {

	public story = <Story> {};

	/**
     * constructor
     *
     * @param {type}  public navCtrl: NavController  
     * @param {type}  private navParams: NavParams
     * @param {type}  public storyService: StoryService
     * @param {type}  private loader: LoaderService
     */
	constructor(public navCtrl: NavController, private navParams: NavParams, private storyService: StoryService, private loader: LoaderService) {
	}
    
    /**
     * ionViewDidLoad
     */
	ionViewDidLoad() {
		this.getStory();
	}

	/**
	 * onSwipe
	 *
     * On tab swipe redirect to pages
     *
     * @param  {type}  event
     */
	onSwipe(event) {
		if(event.direction === 2) {
			this.navCtrl.parent.select(5);
		} 
		if(event.direction === 4) {
			this.navCtrl.parent.select(3);
		}
    }

    /**
	 * Get story
     */
	getStory() {
		let param = this.navParams.get('id');
        this.loader.show();
        this.storyService.get(param).subscribe((response) => {
            this.loader.hide();
            if(response.status) {
            	this.story = response.data;
            }
        }, (error) => {
            this.loader.hide();
        });
	}

}
