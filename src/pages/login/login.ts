import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { AuthService, StorageService, LoaderService, ToastService } from './../../providers';

@IonicPage()
@Component({
	selector: 'page-login',
	templateUrl: 'login.html'
})
export class LoginPage {

	public user = {
		email: '',
		password: ''
	};

	/**
     * constructor
     *
     * @param {type}  public navCtrl: NavController  
     * @param {type}  private authService: AuthService
     * @param {type}  private storage: StorageService
     * @param {type}  private loader: LoaderService
     * @param {type}  private toast: ToastService
     */
	constructor(public navCtrl: NavController, private authService: AuthService, private storage: StorageService, private loader: LoaderService, private toast: ToastService) {
	}
    
    /**
     * doLogin
     *
     * do login
     * set validation
     * if login success set localStorage data
     * navigate to TabsPage
     * otherwise show toast message
     *
     * @param {type}  user
     */
	doLogin(user: any) {
		if(user.email == "") {
			this.toast.custom('Please enter your email address');
		} else if(user.password == "") {
			this.toast.custom('Please enter your password');
		} else {
			let param = {
		        email: user.email,
		        password: user.password
		    };
			this.loader.show();
			this.authService.doLogin(param).subscribe((response) => {
				this.loader.hide();
				if(response.status == "success") {
					this.user = {
						email: '',
						password: ''
					};
					let user = {
						teamId: response.team_id,
						teamName: response.team_name
					};
					this.storage.set("isLoggedIn", 'true');
					this.storage.set("authToken", response.auth_token);
					this.storage.set("userData", JSON.stringify(user));
					this.navCtrl.setRoot('TabsPage');
				} else {
					this.toast.custom(response.message);
				}
			}, (error) => {
				this.loader.hide();
				this.toast.custom(JSON.parse(error._body).message);
			});
		}
	}

}
