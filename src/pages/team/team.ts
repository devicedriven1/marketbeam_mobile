import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, ModalController, PopoverController, Content } from 'ionic-angular';
import { FormControl } from '@angular/forms';
import { CommonService, TeamService, StorageService, LoaderService, ToastService } from './../../providers';

@IonicPage()
@Component({
	selector: 'page-team',
	templateUrl: 'team.html'
})
export class TeamPage {

	@ViewChild('pageTop') pageTop: Content;
	public showEmptyPage: boolean = false;
	private scrollToTopEnabled: boolean = true;
	public isInfiniteEnable: boolean = true;
	public searchText: string = '';
	public searchControl: FormControl;
	private offset: number = 0;
	private pageNo: number = 1;
	private teamId: any;
	private filterItem: any;
	private overview: any;
	public users: any[];
	private usersTemp: any;
	public showCustomFab: boolean = false;
	
	/**
     * constructor
     *
     * @param {type}  public navCtrl: NavController 
     * @param {type}  public modalCtrl: ModalController
     * @param {type}  public popoverCtrl: PopoverController
     * @param {type}  public commonService: CommonService
     * @param {type}  private teamService: TeamService
     * @param {type}  private storage: StorageService
     * @param {type}  private loader: LoaderService
     * @param {type}  private toast: ToastService 
     */
	constructor(public navCtrl: NavController, public modalCtrl: ModalController, public popoverCtrl: PopoverController, public commonService: CommonService, private teamService: TeamService, private storage: StorageService, private loader: LoaderService, private toast: ToastService) {
		this.searchControl = new FormControl();
	}
    
    /**
     * ionViewDidLoad
     */
	ionViewDidLoad() {
		let user    = JSON.parse(this.storage.get('userData'));
		this.teamId = user.teamId;

		this.filterItem = { 
			title: 'All Users', 
			value: 'all' 
		};

		this.overview = {
        	totalUsers: 0,
        	adminUsers: 0,
        	usersNotConfirmed: 0,
			inviteLink: ''
		};

		this.setFilteredItems();
		this.searchControl.valueChanges.debounceTime(700).subscribe(search => {
			this.setFilteredItems();
		});
	}

	/**
	 * onSwipe
	 *
     * On tab swipe redirect to pages
     *
     * @param  {type}  event
     */
	onSwipe(event) {
		if(event.direction === 2) {
			this.navCtrl.parent.select(5);
		} 
		if(event.direction === 4) {
			this.navCtrl.parent.select(3);
		}
    }

    /**
	 * clearData
     */
	clearData() {
		this.offset = 0;
		this.pageNo = 1;
		this.users = [];
        this.usersTemp = [];
		this.getUsers();
	}
    
    /**
	 * presentPopover
	 *
	 * @return {type}  event
	 */
    presentPopover(event) {
		let popover = this.popoverCtrl.create('UserFilterPopoverPage');
		popover.present({
			ev: event
		});
		popover.onDidDismiss((data) => {
			if(data) {
				this.filterItem = data;
				this.clearData();
			}
		});
	}

	/**
	 * set filtered items
	 *
	 * DON'T filter the users IF the supplied input is an empty string
	 *
	 * @return {type}  users
	 */
	setFilteredItems() {
		this.getUsers();
	}

	/**
     * Get stories
     *
     * @return {type}  stories
     */
	getUsers() {
		this.showEmptyPage = false;
		this.isInfiniteEnable = true;
        this.users = [];
        this.usersTemp = [];

        let param = {
        	teamId: this.teamId,
        	pageNo: this.pageNo,
        	perPage: 10,
        	search: this.searchText,
        	filter: this.filterItem != undefined ? this.filterItem.value : '' 
        };

        this.loader.show();
        this.teamService.filterTeam(param).subscribe((response) => {
            this.loader.hide();
            this.showEmptyPage = true;
            if(response) {
            	if(response.objects.length < 10) {
			        this.isInfiniteEnable = false;
		        } else {
			        this.offset = this.offset + 10;
			        this.pageNo = this.pageNo+1;
		        }

		        this.users = response.objects;
            	this.usersTemp = response.objects;
            	this.overview = {
		        	totalUsers: response.users_total,
		        	adminUsers: response.users_admins,
		        	usersNotConfirmed: response.users_not_confirmed,
					inviteLink: response.team.inviteLink
				};
            }
        }, (error) => {
            this.loader.hide();
            this.showEmptyPage = true;
        });
		
		if(this.scrollToTopEnabled) {
			this.scrollToTopEnabled = false;
			this.pageTop.scrollToTop(500);
			setTimeout(() => {
				this.scrollToTopEnabled = true;
			}, 500)
		}
	}
    
    /**
	 * doInfinite
	 *
     * Get list on page scroll
     *
     * @param  {type}  infiniteScroll
     */
	doInfinite(infiniteScroll): Promise<any> {
        return new Promise((resolve) => {
		    setTimeout(() => {
		    	let param = {
		        	teamId: this.teamId,
		        	pageNo: this.pageNo,
		        	perPage: 10,
		        	search: this.searchText,
		        	filter: this.filterItem.value
		        };

		        this.loader.show();
		        this.teamService.filterTeam(param).subscribe((response) => {
		            this.loader.hide();
		            if(response) {
						if(response.objects.length < 10) {
							this.isInfiniteEnable = false;
						} else {
							this.offset = this.offset + 10;
							this.pageNo = this.pageNo+1;
						}

						this.users = this.users.concat(response.objects);
                        this.usersTemp = this.usersTemp.concat(response.objects);
						this.overview = {
				        	totalUsers: response.users_total,
				        	adminUsers: response.users_admins,
				        	usersNotConfirmed: response.users_not_confirmed,
							inviteLink: response.team.inviteLink
						};
					}
		        }, (error) => {
		            this.loader.hide();
		        });
				infiniteScroll.complete();
		    }, 500);
		})
	}
    
    /**
	 * doRefresh
	 *
     * List refresh on each tab
     *
     * @param  {type}  refresher
     */
	doRefresh(refresher) {
		setTimeout(() => {
			this.clearData();
			refresher.complete();
		}, 2000);
	}

	/**
	 * openModal
     *
     * @param  {type}  page
     * @param  {type}  fab
     */
	openModal(page, fab) {
		fab.close();
		const modal = this.modalCtrl.create(page, {
			overview: this.overview
		});
        modal.present();
        modal.onDidDismiss(data => {
        	if(data == 'added') {
			    this.clearData();
			}
	    });
	}

	/**
	 * makeAdmin
     *
     * @param  {type}  user
     */
	makeAdmin(user) {
		this.loader.show();
        this.teamService.makeAdmin(user.userid, this.teamId).subscribe((response) => {
            this.loader.hide();
            if(response) {
            	this.toast.custom('Updated successfully');
			}
        }, (error) => {
            this.loader.hide();
        });
	}

	/**
	 * removeAdmin
     *
     * @param  {type}  user
     */
	removeAdmin(user) {
		this.loader.show();
        this.teamService.removeAdmin(user.userid, this.teamId).subscribe((response) => {
            this.loader.hide();
            if(response) {
            	this.toast.custom('Updated successfully');
            	this.clearData();
			}
        }, (error) => {
            this.loader.hide();
        });
	}

	/**
	 * makeInfluencer
     *
     * @param  {type}  user
     */
	makeInfluencer(user) {
		this.loader.show();
        this.teamService.makeInfluencer(user.userid, this.teamId).subscribe((response) => {
            this.loader.hide();
            if(response) {
            	this.toast.custom('Updated successfully');
            	this.clearData();
			}
        }, (error) => {
            this.loader.hide();
        });
	}

	/**
	 * removeInfluencer
     *
     * @param  {type}  user
     */
	removeInfluencer(user) {
		this.loader.show();
        this.teamService.removeInfluencer(user.userid, this.teamId).subscribe((response) => {
            this.loader.hide();
            if(response) {
            	this.toast.custom('Updated successfully');
            	this.clearData();
			}
        }, (error) => {
            this.loader.hide();
        });
	}

	/**
	 * deleteUser
     *
     * @param  {type}  user
     */
	deleteUser(user) {
		this.loader.show();
        this.teamService.delete(user.userid, this.teamId).subscribe((response) => {
            this.loader.hide();
            if(response) {
            	this.toast.custom('Deleted successfully');
            	this.clearData();
			}
        }, (error) => {
            this.loader.hide();
        });
	}

	/**
	 * reInviteUser
     *
     * @param  {type}  user
     */
	reInvite(user) {
		let param = {
            emails: user.email.split(','),
            teamid: this.teamId,
            reinvite: true
        };
        this.loader.show();
        this.teamService.addNew(param).subscribe((response) => {
            this.loader.hide();
            if(response) {
                this.toast.custom('Reinvite successfully');
            }
        }, (error) => {
            this.loader.hide();
            this.toast.custom(JSON.parse(error._body).message);
        });
	}

}
