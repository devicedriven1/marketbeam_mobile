import { Component } from '@angular/core';
import { IonicPage, NavController, App } from 'ionic-angular';
import { AuthService, StorageService, LoaderService, ToastService } from './../../providers';

@IonicPage()
@Component({
	selector: 'page-settings',
	templateUrl: 'settings.html'
})
export class SettingsPage {

	/**
     * constructor
     *
     * @param {type}  public navCtrl: NavController  
     * @param {type}  private auth: AuthService 
     * @param {type}  private storage: StorageService  
     * @param {type}  private loader: LoaderService 
     * @param {type}  private toast: ToastService  
     */
	constructor(public navCtrl: NavController, private app: App, private auth: AuthService, private storage: StorageService, private loader: LoaderService, private toast: ToastService) {
	}

	/**
	 * onSwipe
	 *
     * On tab swipe redirect to pages
     *
     * @param  {type}  event
     */
	onSwipe(event) {
		if(event.direction === 4) {
			this.navCtrl.parent.select(4);
		}
    }

    /**
	 * logout from application
	 *
	 * while user logout please clear | remove all localStorage data
	 * after clear please navigate to login screen
	 */
	doLogout() {
		this.loader.show();
		this.auth.doLogout(null).subscribe((response) => {
			this.loader.hide();
			if(response.status == "success") {
				this.storage.remove('isLoggedIn');
				this.storage.remove('authToken');
				this.storage.remove('userData');
				this.app.getRootNav().setRoot('LoginPage');
			} else {
				this.toast.custom(response.message);
			}
		}, (error) => {
			this.loader.hide();
			this.toast.custom(JSON.parse(error._body).message);
		});
	}

}
