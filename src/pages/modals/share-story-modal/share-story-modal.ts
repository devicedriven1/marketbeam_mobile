import { Component } from '@angular/core';
import { IonicPage, ViewController, NavParams } from 'ionic-angular';
import { StoryService, UserService, StorageService, LoaderService, ToastService } from './../../../providers';

@IonicPage()
@Component({
	selector: 'page-share-story-modal',
	templateUrl: 'share-story-modal.html'
})
export class ShareStoryModalPage {

    private teamId: any;
    private postId: any;
    private searchLinkedinTimeout = 0;
    public connections: any[];
    public linkedinUsers: any[];
    public linkedinUsersTemp: any[];
    public linkedinOrganizations: any[];
    public activeMessageTab: number = 0;
    public showLinkedinUsers: boolean = false;
    public story = {
        id: null,
        url: '',
        image: '',
        title: '',
        description: '',
        isShareEnabled: true,
        customMessage: {
            facebook: '',
            twitter: '',
            linkedin: []
        },
        contentTypes: [],
        scheduledDates: []
    };

	/**
     * constructor
     *
     * @param {type}  public viewCtrl: ViewController
     * @param {type}  private navParams: NavParams
     * @param {type}  private storyService: StoryService
     * @param {type}  private userService: UserService
     * @param {type}  private storage: StorageService
     * @param {type}  private loader: LoaderService
     * @param {type}  private toast: ToastService
     */
	constructor(public viewCtrl: ViewController, private navParams: NavParams, private storyService: StoryService, private userService: UserService, private storage: StorageService, private loader: LoaderService, private toast: ToastService) {
	}
    
    /**
     * ionViewDidLoad
     */
	ionViewDidLoad() {
        let user = JSON.parse(this.storage.get('userData'));
        this.teamId = user.teamId;

        this.getConnections();
        this.getLinkedinUsers(this.teamId);
        this.searchLinkedinOrganization("");

        if(this.navParams.get('id')) {
            let param = this.navParams.get('id');
            this.postId = param;
        }
	}

    /**
     * closeModal
     *
     * Close share story modal
     */
	closeModal() {
		this.viewCtrl.dismiss();
	}
    
    /**
     * getConnections
     *
     * get all channels and subchannels
     */
    getConnections() {
        this.connections = [];
        this.loader.show();
        this.userService.getAllChannel().subscribe((channelResponse) => {
            this.loader.hide();
            if(channelResponse) {
                for(let i = 0; i < channelResponse.objects.length; i++) {
                    channelResponse.objects[i].provider = channelResponse.objects[i].name;
                    channelResponse.objects[i].userName = channelResponse.objects[i].user_name;
                    this.connections.push(channelResponse.objects[i]);
                }

                this.userService.getAllSubChannel().subscribe((subChannelResponse) => {
                    if(subChannelResponse) {
                        for(let i = 0; i < subChannelResponse.objects.length; i++) {
                            subChannelResponse.objects[i].connected = true;
                            subChannelResponse.objects[i].provider  = subChannelResponse.objects[i].name;
                            subChannelResponse.objects[i].userName  = subChannelResponse.objects[i].subchannel_name;
                            this.connections.push(subChannelResponse.objects[i]);
                        }
                    }
                }, (error) => {
                });
            }
        }, (error) => {
            this.loader.hide();
        });
    }

    /**
     * Get linkedin users
     *
     * @param {type}  teamId
     */
    getLinkedinUsers(teamId: any) {
        this.loader.show();
        this.storyService.getLinkedinUsers(teamId).subscribe((response) => {
            this.loader.hide();
            if(response) {
                this.linkedinUsers = [];
                this.linkedinUsersTemp = [];
                for(let i = 0; i < response.objects.length; i++) {
                    this.linkedinUsers.push({
                        provider_id: response.objects[i].provider_id,
                        image_url: response.objects[i].image_url,
                        provider_user_id: response.objects[i].provider_user_id,
                        display_name: response.objects[i].display_name,
                        displayName: '@'+response.objects[i].display_name
                    });
                    this.linkedinUsersTemp = this.linkedinUsers;
                }
            }
        }, (error) => {
            this.loader.hide();
        });
    }

    /**
     * Get linkedin organizations
     *
     * @param {type}  searchParam
     */
    searchLinkedinOrganization(searchParam: any) {
        clearTimeout(this.searchLinkedinTimeout);
        this.searchLinkedinTimeout = setTimeout(() => {
            if(searchParam) {
                let param = searchParam.replace('@', '');
                this.loader.show();
                this.storyService.searchLinkedinOrganization(param).subscribe((response) => {
                    this.loader.hide();
                    if(response) {
                        let linkedinUsersTemp = this.linkedinUsersTemp;
                        this.linkedinUsers = linkedinUsersTemp;
                        for(let i = 0; i < response.result.length; i++) {
                            this.linkedinUsers.push({
                                provider_id: response.result[i].provider_id,
                                image_url: response.result[i].image_url,
                                provider_user_id: response.result[i].provider_user_id,
                                display_name: response.result[i].display_name,
                                displayName: '@'+response.result[i].display_name,
                                connected: true
                            });
                        }
                    }
                }, (error) => {
                    this.loader.hide();
                    this.linkedinUsers = this.linkedinUsersTemp;
                });
            } else {
                this.linkedinUsers = this.linkedinUsersTemp;
            }
        }, 1500);
    }
    
    /**
     * setMessage
     *
     * @param  {type}  tab
     * @return {type}  activeMessageTab
     */
    setMessage(tab) {
        this.activeMessageTab = tab;
    }

    /**
     * onConnectionChange
     *
     * @param  {type}  index
     * @return {type}  
     */
    onConnectionChange(index) {
        this.connections[index].connected = !this.connections[index].connected;
    }
    
    /**
     * setConnections
     *
     * @return {type}  connections
     */
    setConnections() {
        let connections = [];
        for(let i = 0; i < this.connections.length; i++) {
            if(this.connections[i].connected == true) {
                connections.push(this.connections[i]);
            }
        }
        return connections;
    }
    
    /**
     * share
     */
    share() {
        let param = {
            channels: this.setConnections(),
            post_id: this.postId,
            team_id: this.teamId
        };
        this.loader.show();
        this.storyService.share(param).subscribe((response) => {
            this.loader.hide();
            if(response) {
                this.toast.custom('Successfully posted');
            }
        }, (error) => {
            this.loader.hide();
        });
    }

}
