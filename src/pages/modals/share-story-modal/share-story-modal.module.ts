import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShareStoryModalPage } from './share-story-modal';
import { TagInputModule } from 'ngx-chips';

@NgModule({
	declarations: [
		ShareStoryModalPage
	],
	imports: [
		TagInputModule,
		IonicPageModule.forChild(ShareStoryModalPage)
	]
})
export class ShareStoryModalPageModule {}
