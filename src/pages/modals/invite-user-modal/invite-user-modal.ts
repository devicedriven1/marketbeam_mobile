import { Component } from '@angular/core';
import { IonicPage, ViewController, Platform, NavParams } from 'ionic-angular';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Clipboard } from '@ionic-native/clipboard';
import { ToastService } from './../../../providers';

@IonicPage()
@Component({
	selector: 'page-invite-user-modal',
	templateUrl: 'invite-user-modal.html'
})
export class InviteUserModalPage {

    public inviteLink: any;

	/**
     * constructor
     *
     * @param {type}  public viewCtrl: ViewController
     * @param {type}  public platform: Platform
     * @param {type}  private navParams: NavParams
     * @param {type}  private socialSharing: SocialSharing
     * @param {type}  private clipboard: Clipboard
     * @param {type}  private toast: ToastService
     */
	constructor(public viewCtrl: ViewController, public platform: Platform, private navParams: NavParams, private socialSharing: SocialSharing, private clipboard: Clipboard, private toast: ToastService) {
	}
    
    /**
     * ionViewDidLoad
     */
	ionViewDidLoad() {
        let overview = this.navParams.get('overview');
        this.inviteLink = overview.inviteLink;
	}
    
    /**
     * closeModal
     *
     * Close invite user modal
     */
	closeModal() {
		this.viewCtrl.dismiss();
	}

    /**
     * copyLink
     *
     * copy invite link
     */
    copyLink() {
        this.clipboard.copy(this.inviteLink);
        this.toast.custom('Copied');
    }
    
    /**
     * doInvite
     *
     * invite users through email as provided
     * check the application working platform
     * if its in mobile use shareViaEmail
     * otherwise use mailto open method
     */
    doInvite() {
        let platform = this.platform.is('cordova') ? 'mobile' : 'web';
        if(platform == 'mobile') {
            this.socialSharing.shareViaEmail(this.inviteLink, 'Invite Link', []).then(() => {
                this.toast.custom('Invite success');
                this.viewCtrl.dismiss();
            }).catch(() => {
                this.toast.custom('Invite failed');
            });
        } else {
            window.open('mailto:?subject=InviteLink&body='+this.inviteLink, '_blank');
        }
    }

}
