import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InviteUserModalPage } from './invite-user-modal';

@NgModule({
	declarations: [
		InviteUserModalPage
	],
	imports: [
		IonicPageModule.forChild(InviteUserModalPage)
	]
})
export class InviteUserModalPageModule {}
