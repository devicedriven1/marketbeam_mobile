import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LinkedinModalPage } from './linkedin-modal';

@NgModule({
	declarations: [
		LinkedinModalPage
	],
	imports: [
		IonicPageModule.forChild(LinkedinModalPage)
	]
})
export class LinkedinModalPageModule {}
