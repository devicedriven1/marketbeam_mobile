import { Component } from '@angular/core';
import { IonicPage, ViewController } from 'ionic-angular';
import { UserService, LoaderService, ToastService } from './../../../providers';

@IonicPage()
@Component({
	selector: 'page-linkedin-modal',
	templateUrl: 'linkedin-modal.html'
})
export class LinkedinModalPage {

	public pages: any[];
	public linkedinPage = {
        id: "",
        name: "",
        provider_user_id: "",
        connected: false
    };

	/**
     * constructor
     *
     * @param {type}  public viewCtrl: ViewController
     * @param {type}  private userService: UserService
     * @param {type}  private loader: LoaderService
     * @param {type}  private toast: ToastService
     */
	constructor(public viewCtrl: ViewController, private userService: UserService, private loader: LoaderService, private toast: ToastService) {
	}

	/**
     * ionViewDidLoad
     */
	ionViewDidLoad() {
		this.getLinkedinPages();
	}
    
    /**
     * getLinkedinPages
     *
     * @return {type}  pages
     */
	getLinkedinPages() {
		this.loader.show();
        this.userService.getLinkedinPages().subscribe((response) => {
            this.loader.hide();
            if(response) {
            	this.pages = response.objects
            }
        }, (error) => {
            this.loader.hide();
        });
	}
    
    /**
     * closeModal
     * 
     * Close add connection modal
     */
	closeModal() {
		this.viewCtrl.dismiss();
	}
   
    /**
	 * addLinkedinPage
	 *
	 * @param {type}  page
     */
    addLinkedinPage(linkedinPage) {
    	let param = {
            id: linkedinPage.id,
            name: linkedinPage.name,
            provider_id: "linkedinpage",
            provider_user_id: linkedinPage.provider_user_id
		};
    	this.loader.show();
        this.userService.addLinkedinPage(param).subscribe((response) => {
            this.loader.hide();
            if(response) {
            	this.toast.custom('Added successfully');
    	        this.viewCtrl.dismiss('success');
            }
        }, (error) => {
            this.loader.hide();
        });
    }

}
