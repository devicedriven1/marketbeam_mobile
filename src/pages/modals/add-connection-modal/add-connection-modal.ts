import { Component } from '@angular/core';
import { IonicPage, ViewController, Platform } from 'ionic-angular';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import { TwitterConnect } from '@ionic-native/twitter-connect';
import { OauthService, UserService, StorageService, LoaderService, ToastService } from './../../../providers';
import * as $ from "jquery";
declare let OAuth: any;

@IonicPage()
@Component({
	selector: 'page-add-connection-modal',
	templateUrl: 'add-connection-modal.html'
})
export class AddConnectionModalPage {

	oAuthId = 'OZLJykN4HnVbgLsqU513cKhnUiY';
    socialMedia: any;
    connection: any;
    showConnection: boolean = false;

	/**
     * constructor
     *
     * @param {type}  public viewCtrl: ViewController
     * @param {type}  public platform: Platform
     * @param {type}  private fb: Facebook  
     * @param {type}  private twitter: TwitterConnect
     * @param {type}  private oauthService: OauthService
     * @param {type}  private userService: UserService
     * @param {type}  private storage: StorageService
     * @param {type}  private loader: LoaderService
     * @param {type}  private toast: ToastService
     */
	constructor(public viewCtrl: ViewController, public platform: Platform, private fb: Facebook, private twitter: TwitterConnect, private oauthService: OauthService, private userService: UserService, private storage: StorageService, private loader: LoaderService, private toast: ToastService) {
	}
    
    /**
     * closeModal
     * 
     * Close add connection modal
     */
	closeModal() {
		this.viewCtrl.dismiss();
	}
    
    /**
	 * Connect to social media
	 *
	 * check social media 
	 * check platform that currently running
     */
    connectSocialMedia() {
    	let platform = this.platform.is('cordova') ? 'mobile' : 'web';
    	if(this.socialMedia == "facebook") {
    		this.doFacebookConnect(platform);
    	} else if(this.socialMedia == "twitter") {
    		this.doTwitterConnect(platform);
    	} else {
    		this.doLinkedinConnect(platform);
    	}
    }

    /**
	 * do facebook connect
	 *
	 * check platform 
	 * if it is running in mobile set fb cordova plugin
	 * otherwise set OAuth.io facebook connect
	 *
	 * define the default scope values
	 * get permission from facebook
	 * get profile details if user get valid access token
	 * save user details in localStorage
	 *
	 * @param {type}  platform
     */
    doFacebookConnect(platform) {
    	if(platform == 'mobile') {
		    // Login with permissions
		    // the permissions your facebook app needs from the user
		    let permissions = new Array<string>();
		        permissions = ['public_profile', 'email'];
		    
		    this.fb.login(permissions)
		    .then((res: FacebookLoginResponse) => {

		        // The connection was successful
		        if(res.status == "connected") {

		            // Get user ID and Token
		            var fbId    = res.authResponse.userID;
		            var fbToken = res.authResponse.accessToken;

		            // Get user infos from the API
		            this.fb.api("/me?fields=name,email", []).then((data) => {
		            	self.showConnection = true;
		                self.connection = {
				    		id: fbId,
							name: data.name,
							email: data.email,
							phone: '',
							image: "https://graph.facebook.com/" + fbId + "/picture?type=large",
							accessToken: fbToken,
							secret: '',
							provider: 'facebookpage'
					    };
		            });
		        } else {
		            console.log("An error occurred...");
		        }
		    })
		    .catch((e) => {
		        console.log('Error logging into Facebook', e);
		    });
		} else {
			var self = this;
			OAuth.initialize(this.oAuthId);
			OAuth.popup('facebook', {cache: false}).done(function(result) {
			    result.me().done(function(data) {
				    self.showConnection = true;
				    self.connection = {
			    		id: data.id,
						name: data.name,
						email: data.email,
						phone: '',
						image: data.avatar,
						accessToken: result.access_token,
						secret: '',
						provider: 'facebookpage'
				    };
				})
			})
		}
    } 

    /**
	 * do twitter connect
	 *
	 * check platform 
	 * if it is running in mobile set twitter connect cordova plugin
	 * otherwise set OAuth.io twitter connect
	 *
	 * get permission from twitter
	 * get profile details if user allow to access profile details
	 * save user details in localStorage
	 *
	 * @param {type}  platform
     */
    doTwitterConnect(platform) {
    	if(platform == 'mobile') {
	    	let self = this;
	    	this.twitter.login().then(response => {
	    		this.showConnection = true;
				this.connection = {
		    		id: response.userId,
					name: response.userName,
					email: '',
					phone: '',
					image: "https://twitter.com/"+response.userName+"/profile_image?size=original",
					accessToken: response.token,
					secret: response.secret,
					provider: 'twitter'
			    };
			}, error => {
				console.log(error);
			});
	    } else {
	    	var self = this;
	    	OAuth.initialize(this.oAuthId)
			OAuth.popup('twitter', {cache: false}).done(function(result) {
			    result.me().done(function(data) {
			    	self.showConnection = true;
				    self.connection = {
			    		id: data.id,
						name: data.name,
						email: '',
						phone: '',
						image: data.avatar,
						accessToken: result.oauth_token,
						secret: result.oauth_token_secret,
						provider: 'twitter'
				    };
				})
			})
	    }
    }

    /**
	 * do linkedin connect
	 *
	 * check platform 
	 * set OAuth.io linkedin
	 *
	 * get access token from linkedin
	 * get profile details if user allow to access profile details
	 * save user details in localStorage
	 *
	 * @param {type}  platform
     */
	doLinkedinConnect(platform) {
		if(platform == 'mobile') {
	    	let self = this;
	    	this.oauthService.doLinkedInLogin().then(success => {  
	            var cbUrl       = "http://localhost/callback";
	            var successCode = success.code;

	            // api call to get and save linkedin user data
	            var param = {
				   auth_code: successCode
				};
	            self.oauthService.addLinkedin(param).subscribe((response) => {
		            if(response.status == true) {
		            	self.toast.custom('Added successfully');
    	                self.viewCtrl.dismiss('success');
		            }
		        }, (error) => {
		        });
	        }, (error) => {
	            console.log(error);
	        });
	    } else {
			var self = this;

			// Initialize with your OAuth.io app public key
		    OAuth.initialize(this.oAuthId);
			OAuth.popup('linkedin2', {cache: false}).done(function(result) {
		        result.get('/v2/me').then(data => {
		            self.showConnection = true;
				    self.connection = {
			    		id: data.id,
						name: data.firstName.localized.en_US + ' ' + data.lastName.localized.en_US,
						email: '',
						phone: '',
						image: data.profilePicture.displayImage,
						accessToken: result.access_token,
						secret: '',
						provider: self.socialMedia
				    };
		        })
			});
		}
	}
    
    /**
	 * addToProfile
	 *
	 * @param {type}  connection
     */
    addToProfile(connection) {
    	let param = {
		   provider_id: connection.provider,
		   provider_user_id: connection.id,
		   access_token: connection.accessToken,
		   secret: connection.secret,
		   dispaly_name: connection.name,
		   full_name: connection.name,
		   email: connection.email,
		   profile_url: connection.image,
		   image_url: connection.image,
		   version: "V2",
		};
    	this.loader.show();
        this.userService.addConnection(param).subscribe((response) => {
            this.loader.hide();
            if(response) {
            	this.toast.custom('Added successfully');
    	        this.viewCtrl.dismiss('success');
            }
        }, (error) => {
            this.loader.hide();
        });
    }

}
