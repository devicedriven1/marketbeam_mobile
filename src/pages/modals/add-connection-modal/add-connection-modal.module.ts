import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddConnectionModalPage } from './add-connection-modal';

@NgModule({
	declarations: [
		AddConnectionModalPage
	],
	imports: [
		IonicPageModule.forChild(AddConnectionModalPage)
	]
})
export class AddConnectionModalPageModule {}
