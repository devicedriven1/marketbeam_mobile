import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserOverviewModalPage } from './user-overview-modal';

@NgModule({
	declarations: [
		UserOverviewModalPage
	],
	imports: [
		IonicPageModule.forChild(UserOverviewModalPage)
	]
})
export class UserOverviewModalPageModule {}
