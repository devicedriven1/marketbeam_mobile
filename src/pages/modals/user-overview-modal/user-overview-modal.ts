import { Component } from '@angular/core';
import { IonicPage, ViewController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
	selector: 'page-user-overview-modal',
	templateUrl: 'user-overview-modal.html'
})
export class UserOverviewModalPage {

    public overview = {
        totalUsers: 0,
        adminUsers: 0,
        usersNotConfirmed: 0,
        inviteLink: ''
    }

	/**
     * constructor
     *
     * @param {type}  public viewCtrl: ViewController
     * @param {type}  private navParams: NavParams
     */
	constructor(public viewCtrl: ViewController, private navParams: NavParams) {
	}
    
    /**
     * ionViewDidLoad
     */
	ionViewDidLoad() {
        this.overview = this.navParams.get('overview');
	}
    
    /**
     * closeModal
     *
     * Close user overview modal
     */
	closeModal() {
		this.viewCtrl.dismiss();
	}

}
