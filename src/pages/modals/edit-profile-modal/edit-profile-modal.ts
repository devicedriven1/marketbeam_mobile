import { Component } from '@angular/core';
import { IonicPage, ViewController } from 'ionic-angular';
import { UserService, LoaderService, ToastService } from './../../../providers';

@IonicPage()
@Component({
	selector: 'page-edit-profile-modal',
	templateUrl: 'edit-profile-modal.html'
})
export class EditProfileModalPage {
    
	public user = {
    	image: '',
    	firstname: '',
		lastname: '',
		email: '',
		autoshare: false,
		enable_fb_share: false,
		complete: false
    };

	/**
     * constructor
     *
     * @param {type}  public viewCtrl: ViewController
     * @param {type}  private userService: UserService
     * @param {type}  private loader: LoaderService
     * @param {type}  private toast: ToastService
     */
	constructor(public viewCtrl: ViewController, private userService: UserService, private loader: LoaderService, private toast: ToastService) {
	}
    
    /**
     * ionViewDidLoad
     *
     * get profile
     */
	ionViewDidLoad() {
		this.getProfile();
	}
    
    /**
     * closeModal
     *
     * Close edit profile modal
     */
	closeModal() {
		this.viewCtrl.dismiss();
	}

	/**
     * getProfile
     *
	 * get user profile
     */
    getProfile() {
    	this.loader.show();
        this.userService.get().subscribe((response) => {
        	this.loader.hide();
            if(response) {
            	this.user = {
		    		image: '',
		    		firstname: response.firstname,
					lastname: response.lastname,
					email: response.email,
		    		autoshare: response.autoshare,
					enable_fb_share: response.enable_fb_share,
					complete: response.complete
		    	};
            }
        }, (error) => {
            this.loader.hide();
        });
    }

    /**
	 * on change sharing
	 *
	 * change share option
     *
     * @param {type}  autoshare
     */
    onChangeSharing(autoShare) {
    	this.user.autoshare = autoShare;
    }
    
    /**
	 * doSave 
	 *
	 * update user profile
	 *
	 * @param {type}  user
     */
    doSave(user: any) {
    	if(user.firstname == '') {
    		this.toast.custom('Please enter first name');
    	} else if(user.lastname == '') {
    		this.toast.custom('Please enter last name');
    	} else if(user.email == '') {
    		this.toast.custom('Please enter email address');
    	} else {          
            let param = {
            	firstname: user.firstname,
				lastname: user.lastname,
				autoshare: user.autoshare,
				enable_fb_share: user.enable_fb_share
			};
    		this.loader.show();
	        this.userService.update(param).subscribe((response) => {
	            this.loader.hide();
	            this.toast.custom('Profile updated successfully');
	            this.viewCtrl.dismiss('success');
	        }, (error) => {
	            this.loader.hide();
	        });
    	}
    }

}
