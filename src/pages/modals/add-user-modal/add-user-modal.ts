import { Component } from '@angular/core';
import { IonicPage, ViewController } from 'ionic-angular';
import { TeamService, StorageService, LoaderService, ToastService } from './../../../providers';

@IonicPage()
@Component({
	selector: 'page-add-user-modal',
	templateUrl: 'add-user-modal.html'
})
export class AddUserModalPage {

    private teamId: any;
    public user = {
        emails: ''
    };

	/**
     * constructor
     *
     * @param {type}  public viewCtrl: ViewController
     * @param {type}  private teamService: TeamService
     * @param {type}  private storage: StorageService
     * @param {type}  private loader: LoaderService
     * @param {type}  private toast: ToastService
     */
	constructor(public viewCtrl: ViewController, private teamService: TeamService, private storage: StorageService, private loader: LoaderService, private toast: ToastService) {
	}
    
    /**
     * ionViewDidLoad
     *
     * set teamiD
     */
	ionViewDidLoad() {
        let user = JSON.parse(this.storage.get('userData'));
        this.teamId = user.teamId;
	}
    
    /**
     * closeModal
     *
     * Close add new user modal
     */
	closeModal() {
		this.viewCtrl.dismiss();
	}
    
    /**
     * onSave
     *
     * add new user
     * validate user email address
     */
    onSave() {
        if(this.user.emails == '') {
            this.toast.custom('Please enter valid email address');
        } else {
            let param = {
                emails: this.user.emails.split(','),
                teamid: this.teamId,
                reinvite: false
            };
            this.loader.show();
            this.teamService.addNew(param).subscribe((response) => {
                this.loader.hide();
                if(response) {
                    this.toast.custom('Added successfully');
                    this.viewCtrl.dismiss('added');
                }
            }, (error) => {
                this.loader.hide();
                this.toast.custom(JSON.parse(error._body).message);
            });
        }
    }

}
