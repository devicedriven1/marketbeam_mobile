import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StoryModalPage } from './story-modal';
import { TagInputModule } from 'ngx-chips';
import { WebcamModule } from 'ngx-webcam';

@NgModule({
	declarations: [
		StoryModalPage
	],
	imports: [
		TagInputModule,
		WebcamModule,
		IonicPageModule.forChild(StoryModalPage)
	]
})
export class StoryModalPageModule {}
