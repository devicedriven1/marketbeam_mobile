import { Component } from '@angular/core';
import { IonicPage, ViewController, NavParams, Platform, ActionSheetController } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { WebcamImage, WebcamInitError, WebcamUtil } from 'ngx-webcam';
import { ConfigService, StoryService, StorageService, LoaderService, ToastService } from './../../../providers';
import * as $ from "jquery";
import moment from 'moment';

@IonicPage()
@Component({
	selector: 'page-story-modal',
	templateUrl: 'story-modal.html'
})
export class StoryModalPage {
    
    private teamId: any;
    private searchLinkedinTimeout = 0;
    public modalTitle: any;

    public image: any;
	public showWebcam = false; // toggle webcam on/off
	public allowCameraSwitch = true;
	public multipleWebcamsAvailable = false;
	public deviceId: string;
	public videoOptions: MediaTrackConstraints = {};
	public errors: WebcamInitError[] = [];
	public webcamImage: WebcamImage = null; // latest snapshot
	private trigger: Subject<void> = new Subject<void>(); // webcam snapshot trigger
	private nextWebcam: Subject<boolean|string> = new Subject<boolean|string>(); // switch to next / previous / specific webcam; true/false: forward/backwards, string: deviceId

	public contentTypes: any[];
	public vipUsers: any[];
	public linkedinUsers: any[];
	public linkedinUsersTemp: any[];
	public linkedinOrganizations: any[];
	public selectedLinkedinUsers: any[];
	public activeMessageTab: number = 0;
	public showLinkedinUsers: boolean = false;
	public scheduledDates: any[];
	public story = {
		id: null,
		url: '',
		image: '',
		title: '',
		description: '',
		isShareEnabled: true,
		customMessage: {
			facebook: '',
			twitter: '',
			linkedin: []
		},
		contentTypes: [],
		scheduledDates: [],
		messagesFor: 0
	};

	/**
     * constructor
     *
     * @param {type}  public viewCtrl: ViewController
     * @param {type}  private navParams: NavParams
     * @param {type}  private platform: Platform
     * @param {type}  private actionSheetCtrl: ActionSheetController
     * @param {type}  private camera: Camera
     * @param {type}  public storyService: StoryService
     * @param {type}  private storage: StorageService
     * @param {type}  private loader: LoaderService
     * @param {type}  private toast: ToastService
     */
	constructor(public viewCtrl: ViewController, private navParams: NavParams, private platform: Platform, private actionSheetCtrl: ActionSheetController, private transfer: FileTransfer, private file: File, private camera: Camera, private config: ConfigService, private storyService: StoryService, private storage: StorageService, private loader: LoaderService, private toast: ToastService) {
	}
    
    /**
     * ionViewDidLoad
     */
	ionViewDidLoad() {
		let user = JSON.parse(this.storage.get('userData'));
        this.teamId = user.teamId;

		this.contentTypes = [];
		this.vipUsers     = [{id: 0, name: "Everyone"}];
		this.scheduledDates = [];

        this.getContentTypes(this.teamId);
        this.getVipUsers(this.teamId);
        this.getLinkedinUsers(this.teamId);
        this.searchLinkedinOrganization("");

        this.modalTitle = this.navParams.get('title');

        if(this.navParams.get('id')) {
        	let param = this.navParams.get('id');
	        this.loader.show();
	        this.storyService.get(param).subscribe((response) => {
	            this.loader.hide();
	            if(response.status) {
	            	let story = response.data;
	            	this.story = {
						id: story.post_id,
						url: story.link,
						image: story.image_link,
						title: story.title,
						description: story.body,
						isShareEnabled: true,
						customMessage: {
							facebook: story.fbextra,
							twitter: story.twitterextra,
							linkedin: []
						},
						contentTypes: story.typeIds,
						scheduledDates: [],
						messagesFor: 0
					};
					if(response.data.linkedin_mentions.length > 0) {
						this.story.customMessage.linkedin = response.data.linkedin_mentions;
					}
	            }
	        }, (error) => {
	            this.loader.hide();
	        });
        }
	}
    
    /**
     * closeModal
     *
     * Close story modal
     */
	closeModal() {
		this.viewCtrl.dismiss();
	}

	/**
	 * Get content types
	 *
	 * @param {type}  teamId
     */
	getContentTypes(teamId: any) {
        this.loader.show();
        this.storyService.getContentTypes(teamId).subscribe((response) => {
            this.loader.hide();
            if(response) {
            	this.contentTypes = response.objects;
            }
        }, (error) => {
            this.loader.hide();
        });
	}

	/**
	 * Get vip users
	 *
	 * @param {type}  teamId
     */
	getVipUsers(teamId: any) {
        this.loader.show();
        this.storyService.getVipUsers(teamId).subscribe((response) => {
            this.loader.hide();
            if(response) {
            	for(let i = 0; i < response.objects.length; i++) {
            		this.vipUsers.push(response.objects[i]);
            	}
            }
        }, (error) => {
            this.loader.hide();
        });
	}

	/**
	 * Get linkedin users
	 *
	 * @param {type}  teamId
     */
	getLinkedinUsers(teamId: any) {
        this.loader.show();
        this.storyService.getLinkedinUsers(teamId).subscribe((response) => {
            this.loader.hide();
            if(response) {
                this.linkedinUsers = [];
                this.linkedinUsersTemp = [];
				for(let i = 0; i < response.objects.length; i++) {
					this.linkedinUsers.push({
						provider_id: response.objects[i].provider_id,
						image_url: response.objects[i].image_url,
						provider_user_id: response.objects[i].provider_user_id,
						display_name: response.objects[i].display_name,
						displayName: '@'+response.objects[i].display_name
					});
					this.linkedinUsersTemp = this.linkedinUsers;
				}
            }
        }, (error) => {
            this.loader.hide();
        });
	}

	/**
	 * Get linkedin organizations
	 *
	 * @param {type}  searchParam
     */
	searchLinkedinOrganization(searchParam: any) {
		clearTimeout(this.searchLinkedinTimeout);
	    this.searchLinkedinTimeout = setTimeout(() => {
	        if(searchParam) {
				let param = searchParam.replace('@', '');
		        this.loader.show();
		        this.storyService.searchLinkedinOrganization(param).subscribe((response) => {
		            this.loader.hide();
		            if(response) {
		            	let linkedinUsersTemp = this.linkedinUsersTemp;
			            this.linkedinUsers = linkedinUsersTemp;
						for(let i = 0; i < response.result.length; i++) {
							this.linkedinUsers.push({
								provider_id: response.result[i].provider_id,
								image_url: response.result[i].image_url,
								provider_user_id: response.result[i].provider_user_id,
								display_name: response.result[i].display_name,
								displayName: '@'+response.result[i].display_name
							});
						}
					}
		        }, (error) => {
		            this.loader.hide();
		            this.linkedinUsers = this.linkedinUsersTemp;
		        });
		    } else {
		    	this.linkedinUsers = this.linkedinUsersTemp;
		    }
	    }, 1500);
	}

	/**
	 * Fetch url contents
	 *
	 * @param {type}  story
     */
	fetchUrl(story: any) {
        this.loader.show();
        this.storyService.fetchUrl(story.url).subscribe((response) => {
            this.loader.hide();
            if(response) {
            	let data = response.objects;
            	this.story.image = data.images[0].url;
            	this.story.title = data.title;
            	this.story.description = data.description;
            }
        }, (error) => {
            this.loader.hide();
        });
	}

	/**
	 * open action sheet
	 *
	 * check platform and do action based on that
     */
    openActionSheet() {
		let platform = this.platform.is('cordova') ? 'mobile' : 'web';
		let actionSheet = this.actionSheetCtrl.create({
			title: 'Profile image',
			buttons: [{
				text: 'Take Photo',
				handler: () => {
					if(platform == 'mobile') {
						this.takePicture(0);
					} else {
						this.showWebcam = true;
					}
				}
			}, {
				text: platform == 'mobile' ? 'Photo Library': 'Upload Photo',
				handler: () => {
					if(platform == 'mobile') {
						this.takePicture(1);
					} else {
						$('#file').click();
					}
				}
			}, {
				text: 'Cancel',
				role: 'cancel',
				handler: () => {
				}
			}]
		});
		actionSheet.present();
	}
    
    /**
	 * take picture
	 *
	 * @param {type} sourceType
     */
	takePicture(sourceType) {
		let libraryOptions = {
			quality: 100,
			targetWidth: 1000,
			targetHeight: 1000,
			correctOrientation: true,
			destinationType: this.camera.DestinationType.FILE_URI,      
			encodingType: this.camera.EncodingType.JPEG,
			sourceType: this.camera.PictureSourceType.PHOTOLIBRARY
		};
		let cameraOptions  = {
			quality: 100,
			targetWidth: 1000,
			targetHeight: 1000,
			correctOrientation: true,
			destinationType: this.camera.DestinationType.FILE_URI,
			encodingType: this.camera.EncodingType.JPEG,
			mediaType: this.camera.MediaType.PICTURE
		};
		let options = sourceType == 1 ? libraryOptions : cameraOptions;
		this.camera.getPicture(options).then((response) => {
			this.story.image = '';
			this.uploadImage('mobile', response);
		}, (err) => {
			this.toast.custom(err);
		});
	}

	/**
     * Change event listener
     * 
     * @param event
     */
	changeListener($event) : void {
		this.readThis($event.target);
	}

    /**
     * Read
     *
     * Convert image to base64
     * 
     * @param event
     */
	readThis(inputValue: any): void {
		let file: File = inputValue.files[0];
		let myReader: FileReader = new FileReader();
	    if (inputValue.files && inputValue.files[0]) {
			let reader = new FileReader();
			reader.readAsDataURL(inputValue.files[0]); // read file as data url
			reader.onload = (event) => { // called once readAsDataURL is completed
		        this.story.image = '';
		        this.uploadImage('web', event.target['result']);
	        }
	    }
	}
    
    /**
	 * openWebcam
	 *
	 * @return {type}  multipleWebcamsAvailable
     */
	openWebcam() {
		WebcamUtil.getAvailableVideoInputs()
		.then((mediaDevices: MediaDeviceInfo[]) => {
			this.multipleWebcamsAvailable = mediaDevices && mediaDevices.length > 1;
		});
	}
    
    /**
	 * triggerSnapshot
	 *
	 * @return {type}  
     */
	public triggerSnapshot(): void {
		this.trigger.next();
	}
    
    /**
	 * handleInitError
	 *
	 * @param  {type}  error
	 * @return {type}  errors
     */
	public handleInitError(error: WebcamInitError): void {
		this.errors.push(error);
	}
    
    /**
	 * handleImage
	 *
	 * @param  {type}  webcamImage
	 * @return {type}  
     */
	public handleImage(webcamImage: WebcamImage): void {
		this.showWebcam = false;
		this.uploadImage('web', webcamImage.imageAsDataUrl);
	}
    
    /**
	 * cameraWasSwitched
	 *
	 * @param  {type}  deviceId
	 * @return {type}  deviceId
     */
	public cameraWasSwitched(deviceId: string): void {
		this.deviceId = deviceId;
	}
    
    /**
	 * triggerObservable
	 *
	 * @return {type}  trigger
     */
	public get triggerObservable(): Observable<void> {
		return this.trigger.asObservable();
	}
    
    /**
	 * nextWebcamObservable
	 *
	 * @return {type}  nextWebcam
     */
	public get nextWebcamObservable(): Observable<boolean|string> {
		return this.nextWebcam.asObservable();
	}

    /**
     * upload image
     * 
     * @param {type} platform
     * @param {type} file
     */
    uploadImage(platform, file) {
    	if(platform == 'mobile') {
    		this.loader.show();
    		let token = '';
	        if(this.storage.get('authToken')) {
	            token = this.storage.get('authToken');
	        }
    		let baseUrl = this.config.configuration[this.config.configuration.env].baseUrl;
    		let url = baseUrl+"api/upload_to_s3?team_id="+this.teamId;
    		let fileTransfer: FileTransferObject = this.transfer.create();
			let options: FileUploadOptions = {
			    fileKey: 'file',
			    fileName: file.substr(file.lastIndexOf('/') + 1),
			    mimeType: "image/jpeg",
				chunkedMode: false,
				headers: {
                    'Authorization': 'Bearer '+ token
				}
			};
			fileTransfer.upload(file, url, options).then(success => {
				this.loader.hide();
				let response = JSON.parse(success.response);
				if(response.status == 'ok') {
					this.story.image = JSON.parse(success.response).download_url;
					this.toast.custom('Image successfully uploaded.');
				} else {
					this.toast.custom('Failed to upload');
				}
			}, err => {
				this.loader.hide();
				this.toast.custom('Error while uploading file.');
			});
		} else {
			let base64 = JSON.stringify({ file: file });
			this.loader.show();
	        this.storyService.uploadImage(base64, this.teamId).subscribe((response) => {
	            this.loader.hide();
				if(response['status'] == 'ok') {
					this.story.image = response['download_url'];
					this.toast.custom('Image successfully uploaded.');
				} else {
					this.toast.custom('Failed to upload');
				}
	        }, (error) => {
	            this.loader.hide();
				this.toast.custom('Error while uploading file.');
	        });
		}
	}
    
    /**
	 * setCustomMessageTab
	 *
	 * @param  {type}  tab
	 * @return {type}  activeMessageTab
     */
	setCustomMessageTab(tab) {
		this.activeMessageTab = tab;
	}
    
    /**
	 * addNewDateToSchedule
	 *
	 * @param  {type}  story
	 * @return {type}  scheduledDates
     */
	addNewDateToSchedule() {
		this.scheduledDates.push('');
	}
    
    /**
	 * removeSchedule
	 *
	 * @param  {type}  item
	 * @return {type}  scheduledDates
     */
	removeSchedule(item) {
		this.scheduledDates.splice(this.scheduledDates.indexOf(item), 1);
	}

    /**
	 * postStory
	 *
	 * Post new story
	 *
	 * @param {type}  story
     */
    postStory(story: any) {
    	let currentDate = new Date();
    	let currentUTCDate = [moment(currentDate).utc().format('YYYY-MM-DDTHH:mm:ss')+'Z'];
    	let scheduleDates = [];
	    	scheduleDates = this.scheduledDates.length > 0 ? this.scheduledDates : currentUTCDate;

    	let linkedinExtra = '';
    	for(let i = 0 ; i < story.customMessage.linkedin.length; i++) {
    		linkedinExtra += story.customMessage.linkedin[i].displayName;
    	}

        let vipSummary = [];
    	if(story.messagesFor != 0) {
			vipSummary.push({
				id: story.messagesFor,
				fbextra: story.customMessage.facebook,
	            twitterextra: story.customMessage.twitter,
				linkedinextra: linkedinExtra
			})
		}

    	let param = {
    		team_id: this.teamId,
    		link: story.url,
			title: story.title,
			body: story.description,
			image_link: story.image,
			send_email: story.isShareEnabled,
			request_admin_approval: false,
			is_approved: true,
			pub_date: scheduleDates,
			fbextra: vipSummary.length == 0 ? story.customMessage.facebook : '',
            twitterextra: vipSummary.length == 0 ? story.customMessage.twitter : '',
			linkedinextra: linkedinExtra,
			linkedin_mentions: JSON.stringify(story.customMessage.linkedin),
			image_list: [],
			typeIds: story.contentTypes,
			themeIds: [],
			vipSummary: vipSummary
    	};
    	
    	this.loader.show();
        this.storyService.createNew(param).subscribe((response) => {
            this.loader.hide();
            if(response) {
            	this.toast.custom('Successfully posted');
            	this.viewCtrl.dismiss('created');
            }
        }, (error) => {
            this.loader.hide();
        });
    }

}
