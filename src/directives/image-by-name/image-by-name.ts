import { Directive } from '@angular/core';

@Directive({
	selector: '[image-by-name]' 
})
export class ImageByNameDirective {

	constructor() {}

	getImageName(firstName, lastName) {
		var imageName = firstName.charAt(0) + lastName.charAt(0);
		return imageName;
	}

}
