import { NgModule } from '@angular/core';
import { ImageByNameDirective } from './image-by-name/image-by-name';
@NgModule({
	declarations: [ImageByNameDirective],
	imports: [],
	exports: [ImageByNameDirective]
})
export class DirectivesModule {}
