import { Injectable } from '@angular/core';

@Injectable()
export class SharedService {

    public item: any;
    
    /**
     * Constructor  
     */
    constructor() {}
    
    /**
     * Set shared data
     *
     * @param  {type}  data
     */
    setData(data) {
        this.item = data;
    }
    
    /**
     * Get shared data
     *
     * @return {type}  value saved in shared service
     */
    getData() {
        return this.item;
    }

}
