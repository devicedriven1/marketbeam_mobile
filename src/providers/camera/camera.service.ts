import { Injectable } from '@angular/core';
import { Platform, ActionSheetController } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { WebcamImage, WebcamInitError, WebcamUtil } from 'ngx-webcam';
import { ConfigService } from './../app/config.service';
import { StoryService } from './../app/story.service';
import { StorageService } from './../storage/storage.service';
import { LoaderService } from './../loader/loader.service';
import { ToastService } from './../toast/toast.service';
import * as $ from "jquery";

@Injectable()
export class CameraService {

	private teamId: any;
    public image: any;
	public showWebcam = false; // toggle webcam on/off
	public allowCameraSwitch = true;
	public multipleWebcamsAvailable = false;
	public deviceId: string;
	public videoOptions: MediaTrackConstraints = {};
	public errors: WebcamInitError[] = [];
	public webcamImage: WebcamImage = null; // latest snapshot
	private trigger: Subject<void> = new Subject<void>(); // webcam snapshot trigger
	private nextWebcam: Subject<boolean|string> = new Subject<boolean|string>(); // switch to next / previous / specific webcam; true/false: forward/backwards, string: deviceId
    
    /**
     * constructor
     *
     * @param {type}  private platform: Platform
     * @param {type}  private actionSheetCtrl: ActionSheetController
     * @param {type}  private camera: Camera
     * @param {type}  private storage: StorageService
     * @param {type}  private loader: LoaderService
     * @param {type}  private toast: ToastService
     */
	constructor(private platform: Platform, private actionSheetCtrl: ActionSheetController, private transfer: FileTransfer, private file: File, private camera: Camera, private config: ConfigService, private storyService: StoryService, private storage: StorageService, private loader: LoaderService, private toast: ToastService) {
		let user = JSON.parse(this.storage.get('userData'));
        this.teamId = user.teamId;
	}

	/**
	 * createFileName
	 *
	 * Create a new name for the image
     */
    createFileName() {
        let date = new Date().valueOf();
        let text = '';
        let possibleText = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        for (let i = 0; i < 5; i++) {
           text += possibleText.charAt(Math.floor(Math.random() *    possibleText.length));
        }
        // Replace extension according to your media type
        let fileName = date + '.' + text + '.jpeg';
        return fileName;
    }
    
    /**
	 * blobToFile
	 *
	 * @param  {type}  theBlob
     * @param  {type}  fileName
     * @return {type}  theBlob
     */
    blobToFile(theBlob, fileName) {
        var b: any = theBlob;
        //A Blob() is almost a File() - it's just missing the two properties below which we will add
        b.lastModifiedDate = new Date();
        b.name = fileName;

        //Cast to a File() type
        return <File>theBlob;
    }
    
    /**
	 * blobToFile
	 *
	 * @param  {type}  dataURI
     * @return {type}  blob
     */
    dataURItoBlob(dataURI) {
        // const byteString = window.atob(dataURI);
        const byteString = atob(dataURI.replace(/^data:image\/(png|jpeg|jpg);base64,/, ''));
        const arrayBuffer = new ArrayBuffer(byteString.length);
        const int8Array = new Uint8Array(arrayBuffer);
        for (let i = 0; i < byteString.length; i++) {
            int8Array[i] = byteString.charCodeAt(i);
        }
        const blob = new Blob([int8Array], { type: 'image/jpeg' });    
        return blob;
    }

    /**
     * @public
     * @method isCorrectFile
     * @param file  {String}       The file type we want to check
     * @description                Uses a regular expression to check that the supplied file format
     *                             matches those specified within the method
     * @return {any}
     */
    isCorrectFileType(file) {
        return (/^(gif|jpg|jpeg|png)$/i).test(file);
    }

	/**
	 * open action sheet
	 *
	 * check platform and do action based on that
     */
    openActionSheet() {
		let platform = this.platform.is('cordova') ? 'mobile' : 'web';
		let actionSheet = this.actionSheetCtrl.create({
			title: 'Profile image',
			buttons: [{
				text: 'Take Photo',
				handler: () => {
					if(platform == 'mobile') {
						this.takePicture(0);
					} else {
						this.showWebcam = true;
					}
				}
			}, {
				text: 'Photo Library',
				handler: () => {
					if(platform == 'mobile') {
						this.takePicture(1);
					} else {
						$('#file').click();
					}
				}
			}, {
				text: 'Cancel',
				role: 'cancel',
				handler: () => {
				}
			}]
		});
		actionSheet.present();
	}
    
    /**
	 * take picture
	 *
	 * @param {type} sourceType
     */
	takePicture(sourceType) {
		let libraryOptions = {
			quality: 100,
			targetWidth: 1000,
			targetHeight: 1000,
			correctOrientation: true,
			destinationType: this.camera.DestinationType.FILE_URI,      
			encodingType: this.camera.EncodingType.JPEG,
			sourceType: this.camera.PictureSourceType.PHOTOLIBRARY
		};
		let cameraOptions  = {
			quality: 100,
			targetWidth: 1000,
			targetHeight: 1000,
			correctOrientation: true,
			destinationType: this.camera.DestinationType.FILE_URI,
			encodingType: this.camera.EncodingType.JPEG,
			mediaType: this.camera.MediaType.PICTURE
		};
		let options = sourceType == 1 ? libraryOptions : cameraOptions;
		this.camera.getPicture(options).then((response) => {
			this.image = '';
			this.uploadImage('mobile', response);
		}, (err) => {
			this.toast.custom(err);
		});
	}

	/**
     * Change event listener
     * 
     * @param event
     */
	changeListener($event) : void {
		this.readThis($event.target);
	}

    /**
     * Read
     *
     * Convert image to base64
     * 
     * @param event
     */
	readThis(inputValue: any): void {
		let file: File = inputValue.files[0];
		let myReader: FileReader = new FileReader();
	    if (inputValue.files && inputValue.files[0]) {
			let reader = new FileReader();
			reader.readAsDataURL(inputValue.files[0]); // read file as data url
			reader.onload = (event) => { // called once readAsDataURL is completed
		        this.image = '';
		        this.uploadImage('web', event.target['result']);
	        }
	    }
	}

    /**
	 * openWebcam
	 *
	 * @return {type}  multipleWebcamsAvailable
     */
	openWebcam() {
		WebcamUtil.getAvailableVideoInputs()
		.then((mediaDevices: MediaDeviceInfo[]) => {
			this.multipleWebcamsAvailable = mediaDevices && mediaDevices.length > 1;
		});
	}
    
    /**
	 * triggerSnapshot
	 *
	 * @return {type}  
     */
	public triggerSnapshot(): void {
		this.trigger.next();
	}
    
    /**
	 * handleInitError
	 *
	 * @param  {type}  error
	 * @return {type}  errors
     */
	public handleInitError(error: WebcamInitError): void {
		this.errors.push(error);
	}
    
    /**
	 * handleImage
	 *
	 * @param  {type}  webcamImage
	 * @return {type}  
     */
	public handleImage(webcamImage: WebcamImage): void {
		this.showWebcam = false;
		this.uploadImage('web', webcamImage.imageAsDataUrl);
	}
    
    /**
	 * cameraWasSwitched
	 *
	 * @param  {type}  deviceId
	 * @return {type}  deviceId
     */
	public cameraWasSwitched(deviceId: string): void {
		this.deviceId = deviceId;
	}
    
    /**
	 * triggerObservable
	 *
	 * @return {type}  trigger
     */
	public get triggerObservable(): Observable<void> {
		return this.trigger.asObservable();
	}
    
    /**
	 * nextWebcamObservable
	 *
	 * @return {type}  nextWebcam
     */
	public get nextWebcamObservable(): Observable<boolean|string> {
		return this.nextWebcam.asObservable();
	}

	/**
     * upload image
     * 
     * @param {type} platform
     * @param {type} file
     */
    uploadImage(platform, file) {
    	if(platform == 'mobile') {
    		this.loader.show();
    		let token = '';
	        if(this.storage.get('authToken')) {
	            token = this.storage.get('authToken');
	        }
    		let baseUrl = this.config.configuration[this.config.configuration.env].baseUrl;
    		let url = baseUrl+"api/upload_to_s3?team_id="+this.teamId;
    		let fileTransfer: FileTransferObject = this.transfer.create();
			let options: FileUploadOptions = {
			    fileKey: 'file',
			    fileName: file.substr(file.lastIndexOf('/') + 1),
			    mimeType: "image/jpeg",
				chunkedMode: false,
				headers: {
                    'Authorization': 'Bearer '+ token
				}
			};
			fileTransfer.upload(file, url, options).then(success => {
				this.loader.hide();
				let response = JSON.parse(success.response);
				if(response.status == 'ok') {
					this.image = JSON.parse(success.response).download_url;
					this.toast.custom('Image successfully uploaded.');
				} else {
					this.toast.custom('Failed to upload');
				}
			}, err => {
				this.loader.hide();
				this.toast.custom('Error while uploading file.');
			});
		} else {
			let base64 = JSON.stringify({ file: file });
			this.loader.show();
	        this.storyService.uploadImage(base64, this.teamId).subscribe((response) => {
	            this.loader.hide();
				if(response['status'] == 'ok') {
					this.image = response['download_url'];
					this.toast.custom('Image successfully uploaded.');
				} else {
					this.toast.custom('Failed to upload');
				}
	        }, (error) => {
	            this.loader.hide();
				this.toast.custom('Error while uploading file.');
	        });
		}
	}

}
