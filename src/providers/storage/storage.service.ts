import { Injectable } from '@angular/core';
import { Events } from 'ionic-angular';
import * as CryptoJS from 'crypto-js';

@Injectable()
export class StorageService {

    public key = CryptoJS.enc.Base64.parse("#base64Key#");
    public iv  = CryptoJS.enc.Base64.parse("#base64IV#");
    
    /**
     * Constructor 
     *
     * @param  {type}  private events: Events
     *
     */
    constructor(private events: Events) {}

    /**
     * Encrypt data
     *
     * @param  {type}  key
     * @param  {type}  value
     */
    encryptData(key, value) {
        let encrypted = CryptoJS.AES.encrypt(value.toString(), this.key, {iv: this.iv});
        localStorage.setItem(key, encrypted.toString());
    }
    
    /**
     * Decrypt data
     *
     * @param  {type}  key
     * @return {type}  decrypt text
     */
    decryptData(key) {
        if(localStorage.getItem(key) != null || localStorage.getItem(key) != undefined) {
            let encrypted = localStorage.getItem(key);
            let decrypted = CryptoJS.AES.decrypt(encrypted, this.key, {iv: this.iv});
            if(decrypted.sigBytes > 0) {
                try {
                    return decrypted.toString(CryptoJS.enc.Utf8);
                }
                catch(err) {
                    this.events.publish('user:logout');
                    return null;
                }
            } else {
                this.events.publish('user:logout');
                return null;
            }
        } else {
            return null;
        }
    }
    
    /**
     * Save data to localStorage
     *
     * @param  {type}  key
     * @param  {type}  value
     * @return {type}  
     */
    set(key, value) {
        this.encryptData(key, value);
        return true;
    }
    
    /**
     * Get data from localStorage
     *
     * @param  {type}  key
     * @return {type}  value saved in localStorage
     */
    get(key) {
        return this.decryptData(key);
    }
    
    /**
     * Remove localStorage data
     *
     * @param  {type}  key
     * @return {type}  
     */
    remove(key) {
        localStorage.removeItem(key);
        return true;
    }

}