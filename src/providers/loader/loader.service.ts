import { Injectable } from '@angular/core';
import { LoadingController } from 'ionic-angular';

@Injectable()
export class LoaderService {

    public loading: any;
    
    /**
     * Constructor
     *
     * @param {type}  private loadingCtrl: LoadingController  
     */
    constructor(private loadingCtrl: LoadingController) {}
    
    /**
     * Show loader
     */
    show() {
        if (!this.loading) {
            this.loading = this.loadingCtrl.create({
                content: 'Loading...'
            });
            this.loading.present();
        }
    }
    
    /**
     * Hide | dismiss loader
     */
    hide() {
        if (this.loading) {
            this.loading.dismiss();
            this.loading = null;
        }
    }

}
