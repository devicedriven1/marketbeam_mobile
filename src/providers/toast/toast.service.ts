import { Injectable } from '@angular/core';
import { ToastController } from 'ionic-angular';

@Injectable()
export class ToastService {

    public toast: any;
    
    /**
     * Constructor
     *
     * @param {type}  private toastCtrl: ToastController
     */
    constructor(private toastCtrl: ToastController) {}
    
    /**
     * Set custom toast
     *
     * @param {type}  message
     */
    custom(message) {
        this.toast = this.toastCtrl.create({
            message: message,
            duration: 3000,
            position: 'bottom'
        });
        this.toast.present();
    }

}
