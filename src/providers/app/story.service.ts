import { Injectable } from '@angular/core';
import { ApiService } from './api.service';

@Injectable()
export class StoryService {
    
    /**
     * Constructor
     *
     * @param {type}  public api: ApiService
     */
    constructor(private api: ApiService) {}

    /**
     * upload image
     *
     * @param  {type}  param
     * @return {type}  callback 
     */
    uploadImage(base64, teamId) {
        return this.api.post("api/upload_to_s3?pwa=1&team_id="+teamId, base64);
    }

    /**
     * get all stories
     *
     * @param  {type}  options
     * @return {type}  callback 
     */
    getAll(options) {
        return this.api.post("api/post", options);
    }

    /**
     * get story details
     *
     * @param  {type}  id
     * @return {type}  callback 
     */
    get(id) {
        return this.api.get("api/post/"+id);
    }

    /**
     * Fetch content from url
     *
     * @param  {type}  url
     * @return {type}  callback 
     */
    fetchUrl(url) {
        return this.api.get("api/summary?url="+url);
    }

    /**
     * Get content types
     *
     * @param  {type}  teamId
     * @return {type}  callback 
     */
    getContentTypes(teamId) {
        return this.api.get("api/team-content-type?teamid="+teamId);
    }

    /**
     * Get VIP users
     *
     * @param  {type}  teamId
     * @return {type}  callback 
     */
    getVipUsers(teamId) {
        return this.api.get("api/vipusers?teamid="+teamId);
    }

    /**
     * Get linkedin users
     *
     * @param  {type}  teamId
     * @return {type}  callback 
     */
    getLinkedinUsers(teamId) {
        return this.api.get("api/linkedin/search?teamid="+teamId);
    }

    /**
     * search linkedin organization
     *
     * @param  {type}  param
     * @return {type}  callback 
     */
    searchLinkedinOrganization(param) {
        return this.api.get("api/linkedin/organization/search?q="+param);
    }

    /**
     * create new story
     *
     * @param  {type}  data
     * @return {type}  callback 
     */
    createNew(data) {
        return this.api.post("api/create_post", data);
    }

    /**
     * update story
     *
     * @param  {type}  data
     * @return {type}  callback 
     */
    update(data) {
        return this.api.post("api/update_post", data);
    }

    /**
     * approve story
     *
     * @param  {type}  id
     * @param  {type}  data
     * @return {type}  callback 
     */
    approve(id, data) {
        return this.api.patch("api/update_post/"+id, data);
    }
    
    /**
     * delete story
     *
     * @param  {type}  id
     * @param  {type}  data
     * @return {type}  callback 
     */
    delete(id, data) {
        return this.api.put("api/delete_post/"+id, data);
    }

    /**
     * Share story
     *
     * @param  {type}  data
     * @return {type}  callback 
     */
    share(data) {
        return this.api.post("api/share", data);
    }

}