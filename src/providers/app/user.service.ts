import { Injectable } from '@angular/core';
import { ApiService } from './api.service';

@Injectable()
export class UserService {
    
    /**
     * Constructor
     *
     * @param {type}  public api: ApiService
     */
    constructor(private api: ApiService) {}

    /**
     * get profile
     *
     * @return {type}  callback 
     */
    get() {
        return this.api.get("api/profile");
    }

    /**
     * update profile
     *
     * @param  {type}  data
     * @return {type}  callback 
     */
    update(data) {
        return this.api.post("api/profile", data);
    }

    /**
     * get all channels
     *
     * @return {type}  callback 
     */
    getAllChannel() {
        return this.api.get("api/channel");
    }

    /**
     * get all sub channels
     *
     * @return {type}  callback 
     */
    getAllSubChannel() {
        return this.api.get("api/subchannel");
    }

    /**
     * get all sub channel meta
     *
     * @return {type}  callback 
     */
    getAllSubChannelMeta() {
        return this.api.get("api/subchannelmeta");
    }

    /** 
     * get more channels
     *
     * @return {type}  callback 
     */
    getMoreChannel() {
        return this.api.get("api/morechannel");
    }

    /**
     * Add new channel
     *
     * @param  {type}  data
     * @return {type}  callback 
     */
    addChannel(data) {
        return this.api.post("api/channel", data);
    }

    /**
     * Add new connection
     *
     * @param  {type}  data
     * @return {type}  callback 
     */
    addConnection(data) {
        return this.api.post("api/connection/add", data);
    }

    /**
     * Delete connection
     *
     * @param  {type}  providerId
     * @param  {type}  providerUserId
     * @return {type}  callback 
     */
    deleteConnection(providerId, providerUserId) {
        return this.api.delete("api/connect/"+providerId+"/"+providerUserId);
    }

    /** 
     * getLinkedinPages
     *
     * @return {type}  callback 
     */
    getLinkedinPages() {
        return this.api.get("api/lipages");
    }

    /**
     * addLinkedinPage
     *
     * @param  {type}  data
     * @return {type}  callback 
     */
    addLinkedinPage(data) {
        return this.api.post("api/subchannel", data);
    }

}