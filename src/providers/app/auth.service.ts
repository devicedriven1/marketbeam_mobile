import { Injectable } from '@angular/core';
import { ApiService } from './api.service';

@Injectable()
export class AuthService {
    
    /**
     * Constructor
     *
     * @param {type}  public api: ApiService
     */
    constructor(private api: ApiService) {}

    /**
     * Do login
     *
     * @param  {type}  user
     * @return {type}  valid or not 
     */
    doLogin(user) {
        return this.api.post("auth/login", user);
    }

    /**
     * Do logout
     *
     * @param  {type}  data
     * @return {type}  
     */
    doLogout(data) {
        return this.api.post("auth/logout", data);
    }
    
    /**
     * get linkedin token
     *
     * @param  {type}  data
     * @return {type}  
     */
    getLinkedinToken(data) {
        return this.api.post("https://www.linkedin.com/oauth/v2/accessToken?"+data, data);
    }

}