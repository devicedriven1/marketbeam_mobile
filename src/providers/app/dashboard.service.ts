import { Injectable } from '@angular/core';
import { ApiService } from './api.service';

@Injectable()
export class DashboardService {
    
    /**
     * Constructor
     *
     * @param {type}  public api: ApiService
     */
    constructor(private api: ApiService) {}

    /**
     * get summary cards
     *
     * @param  {type}  teamId
     * @return {type}  callback
     */
    getSummary(teamId) {
        return this.api.get("api/stats?teamid="+teamId);
    }

    /**
     * get trends
     *
     * @param  {type}  teamId
     * @param  {type}  days
     * @return {type}  callback
     */
    getTrends(teamId, days) {
        return this.api.get("api/trend?teamid="+teamId+"&days="+days);
    }
    
    /**
     * get best engaging channel
     *
     * @param  {type}  teamId
     * @return {type}  callback
     */
    getBestChannel(teamId) {
        return this.api.get("api/distribution?teamid="+teamId);
    }

    /**
     * get leaderboard
     *
     * @param  {type}  teamId
     * @return {type}  callback
     */
    getLeaderboard(teamId) {
        return this.api.get("api/leaderboard?teamid="+teamId);
    }

    /**
     * get social employee of the month
     *
     * @param  {type}  teamId
     * @param  {type}  month
     * @param  {type}  year
     * @return {type}  callback
     */
    getEmployeeOfMonth(teamId, month, year) {
        return this.api.get("api/top-employee-month?teamid="+teamId+"&month="+month+"&year="+year);
    }

    /**
     * get top posts
     *
     * @param  {type}  teamId
     * @return {type}  callback
     */
    getTopPosts(teamId) {
        return this.api.get("api/topposts?teamid="+teamId);
    }

    /**
     * get click throughs by country
     *
     * @param  {type}  teamId
     * @return {type}  callback
     */
    getClickThroughsByCountry(teamId) {
        return this.api.get("api/ip-geolocations?teamid="+teamId);
    }

    /**
     * getAvgPost
     *
     * @param  {type}  teamId
     * @return {type}  callback
     */
    getAvgPost(teamId) {
        return this.api.get("api/dashboardhighlights?teamid="+teamId);
    }

    /**
     * getTimeSinceLastPost
     *
     * @param  {type}  teamId
     * @return {type}  callback
     */
    getTimeSinceLastPost(teamId) {
        return this.api.get("api/lastpost?teamid="+teamId);
    }

    /**
     * getCompanyChannels
     *
     * @param  {type}  teamId
     * @return {type}  callback
     */
    getCompanyChannels(teamId) {
        return this.api.get("api/socialpages?teamid="+teamId);
    }

}