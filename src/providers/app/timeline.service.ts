import { Injectable } from '@angular/core';
import { ApiService } from './api.service';

@Injectable()
export class TimelineService {
    
    /**
     * Constructor
     *
     * @param {type}  public api: ApiService
     */
    constructor(private api: ApiService) {}
    
    /**
     * get all timelines
     *
     * @param  {type}  param
     * @return {type}  callback 
     */
    getAll(param) {
        if(param.epoch != undefined) {
            return this.api.get("api/timeline?teamid="+param.teamId+"&olderthan="+param.epoch+"&types="+param.types);    
        } else {
            return this.api.get("api/timeline?teamid="+param.teamId+"&types="+param.types);
        }
    }

}