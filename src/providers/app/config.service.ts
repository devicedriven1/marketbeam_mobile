import { Injectable } from '@angular/core';

@Injectable()
export class ConfigService {

	public configuration = null;
    
    /**
     * constructor 
     *
     * Set app configuration
     */
    constructor() {
    	this.configuration = {
    		info: {
		        title: 'Market Beam',
		        version: '0.0.1'
		    },
		    env: 'staging',
		    development: {
		        baseUrl: 'http://192.168.0.251:5000/v0/'
		    },
		    staging: {
		        baseUrl: 'http://slingpost-stagingmobile.herokuapp.com/v0/'
		    },
		    production: {
		        baseUrl: 'http://192.168.0.251:5000/v0/'
		    }
    	}
    }

}