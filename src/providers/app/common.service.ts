import { Injectable } from '@angular/core';
import moment from 'moment';

@Injectable()
export class CommonService {

    constructor() {}
    
    /**
     * getColor
     *
     * @param  {type}  index
     * @param  {type}  random color
     */
    getColor(index) {
        let indexValue = index > 10 ? index.toString()[1] : index;
        let colors = ["#FF0000", "#808000", "#ca6f1e", "#3498db", "#FF00FF", "#800000", "#008080", "#800080", "#c0392b", "#8e44ad", "#3498db"];
        return colors[indexValue];
    }

    /**
     * setProviderName
     *
     * @param  {type}  provider
     * @param  {type}  formatted provider name
     */
    setProviderName(provider) {
        let retVal = '';
        switch(provider) {
            case 'facebook':
                retVal = 'Facebook';
                break;
            case 'facebookpage':
                retVal = 'Facebook Page';
                break;
            case 'linkedin':
                retVal = 'Linkedin';
                break;
            case 'linkedinpage':
                retVal = 'Linkedin Page';
                break;
            case 'twitter':
                retVal = 'Twitter';
                break;
            default:
                // code block
        }

        return retVal;
    }
    
    /**
     * timeAgo
     *
     * @param  {type}  date
     * @param  {type}  formattedDate
     */
    timeAgo(date) {
        return moment(date).fromNow();
    }
    
    /**
     * formatDate
     *
     * @param  {type}  date
     * @param  {type}  formattedDate
     */
    formatDate(date) {
        let dateToChange  = date;
        let formattedDate = moment(dateToChange).format('llll');
        return formattedDate;
    }

    /**
     * getImageName
     *
     * @param  {type}  first
     * @param  {type}  last
     * @return {type}  imageName
     */
    getImageName(first, last) {
        let firstName = first == '' || first == undefined ? 'Anonymous' : first;
        let lastName  = last == '' || last == undefined ? '' : last;

        let findLast  = firstName.split(' ');
        if(findLast[1] !== undefined) {
            lastName = findLast[1];
        }

        let imageName = '';
        if(lastName === undefined) {
            imageName = firstName.charAt(0);
        } else {
            imageName = firstName.charAt(0) + lastName.charAt(0);
        }

        return imageName;
    }

}