import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { ConfigService } from './config.service';
import { StorageService } from './../storage/storage.service';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch'
import 'rxjs/Rx';

@Injectable()
export class ApiService {

    public baseUrl: any;

    /**
     * Constructor
     *
     * @param {type}  public http: Http       
     * @param {type}  private config: ConfigService   
     */
    constructor(public http: Http, public httpClient: HttpClient, private config: ConfigService, private storage: StorageService) {
        this.baseUrl = this.config.configuration[this.config.configuration.env].baseUrl;
    }

    /**
     * Set request header
     *
     * @return {type}  headers
     */
    setHeader() {
        let headers = new Headers();
            headers.append('Content-Type', 'application/json');
            headers.append("Access-Control-Allow-Origin", "*");
            headers.append("Access-Control-Allow-Methods", "GET, POST, PATCH, PUT, DELETE, OPTIONS");
            headers.append("Access-Control-Allow-Headers", "*");

        if(this.storage.get('authToken')) {
            let token = this.storage.get('authToken');
            headers.append('Authorization', 'Bearer '+ token);
        }

        return headers;
    }

    /**
     * Get method
     *
     * @param  {type}  url
     * @return {type}  response
     */
    get(url) {
        let headers = this.setHeader();
        let options = new RequestOptions({ headers: headers });
        return this.http.get(this.baseUrl + url, options)
            .map((res:Response) => res.json());
    }

    /**
     * Post method
     *
     * @param  {type}  url
     * @param  {type}  data
     * @return {type}  response
     */
    post(url, data) {
        let headers = this.setHeader();
        let options = new RequestOptions({ headers: headers });
        let body    = data;
        return this.http.post(this.baseUrl + url, body, options)
            .map((res:Response) => res.json());
    }
    
    /**
     * Post upload
     *
     * @param  {type}  url
     * @param  {type}  data
     * @return {type}  response
     */
    postUpload(url, data) {
        let headers = this.setHeader();
        let options = new RequestOptions({ headers: headers });
        let body    = data;
        return this.http.post(this.baseUrl + url, body, options)
            .map((res:Response) => res.json());
    }

    /**
     * Put method
     *
     * @param  {type}  url
     * @param  {type}  data
     * @return {type}  response
     */
    put(url, data) {
        let headers = this.setHeader();
        let options = new RequestOptions({ headers: headers });
        let body    = data;
        return this.http.put(this.baseUrl + url, body, options)
            .map((res:Response) => res.json());
    }

    /**
     * Patch method
     *
     * @param  {type}  url
     * @param  {type}  data
     * @return {type}  response
     */
    patch(url, data) {
        let headers = this.setHeader();
        let options = new RequestOptions({ headers: headers });
        let body    = data;
        return this.http.patch(this.baseUrl + url, body, options)
            .map((res:Response) => res.json());
    }

    /**
     * Delete method
     *
     * @param  {type}  url
     * @return {type}  response
     */
    delete(url) {
        let headers = this.setHeader();
        let options = new RequestOptions({ headers: headers });
        return this.http.delete(this.baseUrl + url, options)
            .map((res:Response) => res.json());
    }

}
