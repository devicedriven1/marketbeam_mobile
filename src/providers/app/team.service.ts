import { Injectable } from '@angular/core';
import { ApiService } from './api.service';

@Injectable()
export class TeamService {
    
    /**
     * Constructor
     *
     * @param {type}  public api: ApiService
     */
    constructor(private api: ApiService) {}
    
    /**
     * get all team
     *
     * @return {type}  callback 
     */
    getAll() {
        return this.api.get("api/team");
    }

    /**
     * search team
     *
     * @param  {type}  param
     * @return {type}  callback 
     */
    filterTeam(param) {
        return this.api.get("api/user?teamid="+param.teamId+"&page="+param.pageNo+"&results_per_page="+param.perPage+"&search="+param.search+"&filter="+param.filter);
    }

    /**
     * get team details
     *
     * @param  {type}  teamId
     * @return {type}  callback 
     */
    get(teamId) {
        return this.api.get("api/team/"+teamId);
    }

    /**
     * get all team members
     *
     * @param  {type}  teamId
     * @return {type}  callback 
     */
    getAllMembers(teamId) {
        return this.api.get("api/user?teamid="+teamId);
    }

    /**
     * add new user
     *
     * @param  {type}  data
     * @return {type}  callback 
     */
    addNew(data) {
        return this.api.post("api/user", data);
    }

    /**
     * update team
     *
     * @param  {type}  data
     * @return {type}  callback 
     */
    update(data) {
        return this.api.post("api/team", data);
    }

    /**
     * make admin
     *
     * @param  {type}  userId
     * @param  {type}  teamId
     * @return {type}  callback 
     */
    makeAdmin(userId, teamId) {
        return this.api.post("api/adminuser/"+userId+"/team/"+teamId, null);
    }

    /**
     * remove admin
     *
     * @param  {type}  userId
     * @param  {type}  teamId
     * @return {type}  callback 
     */
    removeAdmin(userId, teamId) {
        return this.api.delete("api/adminuser/"+userId+"/team/"+teamId);
    }

    /**
     * make influencer
     *
     * @param  {type}  userId
     * @param  {type}  teamId
     * @return {type}  callback 
     */
    makeInfluencer(userId, teamId) {
        return this.api.post("api/vipuser/"+userId+"/team/"+teamId, null);
    }

    /**
     * remove influencer
     *
     * @param  {type}  userId
     * @param  {type}  teamId
     * @return {type}  callback 
     */
    removeInfluencer(userId, teamId) {
        return this.api.delete("api/vipuser/"+userId+"/team/"+teamId);
    }

    /**
     * approve member
     *
     * @param  {type}  userId
     * @param  {type}  teamId
     * @param  {type}  data
     * @return {type}  callback 
     */
    approve(userId, teamId, data) {
        return this.api.patch("api/user/"+userId+"/team/"+teamId, data);
    }
    
    /**
     * delete member
     *
     * @param  {type}  userId
     * @param  {type}  teamId
     * @return {type}  callback 
     */
    delete(userId, teamId) {
        return this.api.delete("api/user/"+userId+"/team/"+teamId);
    }

}