import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import { TwitterConnect } from '@ionic-native/twitter-connect';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { ApiService } from './../app/api.service';
import { StorageService } from './../storage/storage.service';
import { LoaderService } from './../loader/loader.service';
import { ToastService } from './../toast/toast.service';
import * as $ from "jquery";
declare let OAuth: any;

@Injectable()
export class OauthService {

    private oAuthId  = 'OZLJykN4HnVbgLsqU513cKhnUiY';
    // private clientId = "810fdvazsk7exu";
    private clientId = "75743uw5s3olvc";
    private scope    = "r_liteprofile%20r_emailaddress";
    private cbUrl    = "http://localhost/callback";
    
    /**
     * Constructor
     *
     * @param {type}  private api: ApiService
     * @param {type}  private storage: StorageService
     * @param {type}  private loader: LoaderService
     * @param {type}  private toast: ToastService
     */
    constructor(private iab: InAppBrowser, private api: ApiService, private storage: StorageService, private loader: LoaderService, private toast: ToastService) {}

    /**
     * Make unique
     *
     * @return {type} text
     */
    makeUnique() {
        let text = "";
        let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (let i = 0; i < 5; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }

        return text;
    }
    
    /**
     * doConnect
     *
     * @param {type}  provider
     * @param {type}  url
     */
    doConnect(provider, url) {
        OAuth.popup(provider).then(function(oauthResult) {
            // return oauthResult.get('/me');
            oauthResult.get(url).done(function(data) {
                // todo with data
            }).fail(function(err) {
                // todo with err
            });
        }).then(function(data) {
            // data is the result of the request to /me
        }).fail(function(err) {
            // handle an error
        });
    }

    /**
     * do linkedin login
     *
     * define the default scope values, client id and callback url
     * open the url using inappbrowser
     * get permission from linkedin
     * set authresponse
     */
    doLinkedInLogin(): Promise<any> {
        var self = this;
        return new Promise(function (resolve, reject) {
            var param = "response_type=code&client_id="+self.clientId+"&redirect_uri="+self.cbUrl+"&scope="+self.scope+"&state="+self.makeUnique();
            var browserRef = self.iab.create("https://www.linkedin.com/oauth/v2/authorization?"+param, '_blank', 'location=yes');
            browserRef.on('loadstart').subscribe(event => {
               if ((event.url).indexOf("http://localhost/callback") === 0) {
                    browserRef.on("exit").subscribe(event => {
                    });
                    browserRef.close();

                    var parsedResponse = {};
                    var code  = (event.url.split("=")[1]).split("&")[0];
                    var state = event.url.split("=")[2];
                    if (code !== undefined && state !== null) {
                        parsedResponse["code"] = code;
                        resolve(parsedResponse);
                    } else {
                        reject("Problem authenticating with LinkedIn");
                    }
                }
            });

            browserRef.on("exit").subscribe(event => {
                reject("The LinkedIn sign in flow was canceled");
            });
        });
    }

    /**
     * addLinkedin
     *
     * @param  {type}  code
     * @return {type}  callback 
     */
    addLinkedin(code) {
        return this.api.post("api/connection/li/add", code);
    }

}
