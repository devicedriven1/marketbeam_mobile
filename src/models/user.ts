export interface User {
	firstName: string;
    lastName: string;
    email: string;
    phone: string;
    dob: Date;
    role: string;
    status: boolean;
}